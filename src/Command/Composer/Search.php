<?php

namespace Jarvis\Command\Composer;

use Jarvis\Traits\VaultTrait;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\HttpClient\HttpClient;

class Search extends Command
{
    use VaultTrait;

    const VAULT_SECRET_URL  = "/secret/summa/dev/palantir-access-api";
    const VAULT_KEY_URL     = "url";
    const VAULT_KEY_TOKEN   = "api_key";

    const ARGUMENT_MODULE   = "module";

    /**
     * @var string
     */
    protected static $defaultName = 'composer:search';

    protected function configure(): void
    {
        $this->setDescription('Composer search module')
            ->addArgument(
                self::ARGUMENT_MODULE,
                InputArgument::REQUIRED,
                'search module from private packagist'
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $code = 0;
        $secrets = $this->getVaultSecret(self::VAULT_SECRET_URL);
        $token   = $secrets[self::VAULT_KEY_TOKEN];
        $url     = $secrets[self::VAULT_KEY_URL];
        $url     = is_string($url) ? $url : '';

        $moduleName = $input->getArgument(self::ARGUMENT_MODULE);
        $moduleName = is_string($moduleName) ? $moduleName : '';

        $composerUrl = $url . "/composer/" . $moduleName;

        /**
         * @var \Symfony\Contracts\HttpClient\HttpClientInterface $client
         */
        $client = HttpClient::create(['http_version' => '2.0']);

        $response = $client->request('GET', $composerUrl, [
            'headers' => [
                'Content-Type' => 'application/json',
                'X-API-KEY' => $token,
            ]
        ]);

        if ($response->getStatusCode() == 200) {
            $response = str_replace("\\", "", $response->getContent());
            $output->writeln($response);
        } else {
            $output->writeln(sprintf('<error>Error "%s" connecting to server</error>', $response->getStatusCode()));
            $code = 1;
        }

        return $code;
    }
}
