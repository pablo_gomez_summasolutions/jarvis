<?php

namespace Jarvis\Util\Terraform;

use Jarvis\Traits\LocalStorageTrait;

class HCLParser
{
    use LocalStorageTrait;

    /**
     * json2hcl Version we want to use.
     *
     * @see https://github.com/kvz/json2hcl/releases
     */
    const JSON2HCL_VERSION = '0.0.6';

    /**
     * @var string
     */
    private $hcl;

    /**
     * HCLParser constructor.
     *
     * @param string $hcl
     */
    public function __construct($hcl)
    {
        $this->hcl = $hcl;
    }

    /**
     * @return string
     */
    private function getBinaryPath()
    {
        $binaryKey = $this->getBinaryFilename();
        $binaryPath = $this->getLocalStoragePath($binaryKey);
        if (empty($binaryPath)) {
            $binaryPath = $this->installBinaries();
        }

        return $binaryPath;
    }

    /**
     * @return string
     */
    private function getJSONString()
    {
        $command = $this->getBinaryPath() . ' --reverse <<\'EOF\'' . PHP_EOL . $this->hcl . PHP_EOL . 'EOF';

        exec($command, $lines);

        return implode(PHP_EOL, $lines);
    }

    /**
     * @return mixed
     */
    public function parse()
    {
        return json_decode($this->getJSONString());
    }

    /**
     * Returns the correct binary filename according to the Operating System and Architecture.
     */
    private function getBinaryFilename(): string
    {
        // Defaults
        $osString = 'linux';
        $architecture = 'amd64';

        // We can not test alternative architectures and operating systems, so exclude from code coverage.
        // @codeCoverageIgnoreStart

        // Switch architecture if needed
        if (2147483647 == PHP_INT_MAX) {
            $architecture = '386';
        }

        // Switch Operating System if needed
        switch (true) {
            case stristr(PHP_OS, 'DAR'):
                $osString = 'darwin';
                break;
            case stristr(PHP_OS, 'WIN'):
                $osString = 'windows';
                break;
        }

        // @codeCoverageIgnoreEnd

        return sprintf('json2hcl_v%s_%s_%s', self::JSON2HCL_VERSION, $osString, $architecture);
    }

    private function installBinaries(): string
    {
        $binaryUrls = [
            sprintf(
                'https://github.com/kvz/json2hcl/releases/download/v%s/%s',
                self::JSON2HCL_VERSION,
                self::getBinaryFilename()
            ),
        ];

        foreach ($binaryUrls as $binaryUrl) {
            $destination = $this->getBinaryFilename();

            // Download
            $path = $this->saveLocalStorage($destination, (string) file_get_contents($binaryUrl), 0755, 0);

            return $path;
        }
    }
}
