<?php

namespace Jarvis\Traits;

use \Jarvis\Util\Terraform\HCLParser;

trait TerraformTrait
{
    /**
     * @var mixed
     */
    protected $tfConfig;

    /**
     * @return mixed
     */
    public function getTfConfig()
    {
        if (empty($this->tfConfig)) {
            $tfConfigRaw = '';
            $tfFiles = glob(getcwd() . '/*.tfvars');
            if (is_array($tfFiles)) {
                foreach ($tfFiles as $file) {
                    $tfConfigRaw .= "\n" . file_get_contents($file);
                }
                $this->tfConfig = (new HCLParser($tfConfigRaw))->parse();
            }
        }

        return $this->tfConfig;
    }

    /**
     * @return null|string
     */
    public function getTfEnvironment()
    {
        $tfConfig = $this->getTfConfig();

        return isset($tfConfig->environment) ? $tfConfig->environment : null;
    }


    /**
     * @return null|string
     */
    public function getTfClient()
    {
        $tfConfig = $this->getTfConfig();

        return isset($tfConfig->client_name) ? $tfConfig->client_name : null;
    }

    /**
     * @return null|string
     */
    public function getTfProject()
    {
        $tfConfig = $this->getTfConfig();

        return isset($tfConfig->project_name) ? $tfConfig->project_name : null;
    }
}
