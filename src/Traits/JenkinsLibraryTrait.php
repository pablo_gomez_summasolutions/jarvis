<?php

namespace Jarvis\Traits;

trait JenkinsLibraryTrait
{
    /**
     * @var string[]|null
     */
    protected $jenkinsLibraryInfo;

    /**
     * @return string[]|null
     */
    public function getJenkinsLibraryInfo()
    {
        if ($this->jenkinsLibraryInfo === null) {
            $jenkinsFilePath = getcwd() . '/Jenkinsfile';
            if (file_exists($jenkinsFilePath)) {
                $data = (string)file_get_contents($jenkinsFilePath);
                $this->jenkinsLibraryInfo = $this->parseJenkinsFile($data);
            }
        }

        return $this->jenkinsLibraryInfo;
    }

    /**
     * @param string $content
     * @return string[]
     */
    public function parseJenkinsFile(string $content)
    {
        preg_match("/@Library\('(.*)'\)/", $content, $out);
        $version = 'master';
        if (is_array($out) && is_string($out[1])) {
            $definedVersion = str_replace(['swg-jenkins-library', '@', ""], '', $out[1]);
            if (!empty($definedVersion)) {
                $version = $definedVersion;
            }
        }

        return [
            'version' => $version,
        ];
    }
}
