<?php

namespace Jarvis\Command\DevOps;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputOption;
use Jarvis\Util\Console\Helper\Table\Renderer\RendererFactory;
use Jarvis\Util\Console\Helper\TableHelper;

class Composer extends Command
{
    /**
     * @var string
     */
    protected static $defaultName = 'devops:composer';

    /**
     * @var mixed
     */
    public $moduleList;

    protected function configure(): void
    {
        $this->setDescription('List composer modules')
            ->addOption(
                'vendor',
                null,
                InputOption::VALUE_OPTIONAL,
                'Show modules of a specific vendor'
            )
            ->addOption(
                'format',
                null,
                InputOption::VALUE_OPTIONAL,
                'Output Format. One of [' . implode(',', RendererFactory::getFormats()) . ']'
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $code = 0;
        $vendorFolder = $input->getOption('vendor');
        $autoloadFilePath = getcwd() . '/vendor/composer/installed.json';

        if (is_string($vendorFolder)) {
            $autoloadFilePath = $vendorFolder . '/composer/installed.json';
        }

        if (!file_exists($autoloadFilePath)) {
            $output->writeln(sprintf(
                '<error>Composer file not found at "%s".</error>',
                $autoloadFilePath
            ));

            return 1;
        }

        $moduleList = $this->prepareModuleList($autoloadFilePath);

        $format = $input->getOption('format');
        $format = is_string($format) ? $format : '';

        /* @var $tableHelper TableHelper */
        $tableHelper = new TableHelper($output);
        $tableHelper
            ->setHeaders(['src', 'name', 'version', 'type', 'keywords'])
            ->renderByFormat($output, $moduleList, $format);

        return $code;
    }

    /**
     * @param string $vendor
     * @return mixed
     */
    public function prepareModuleList(string $vendor)
    {
        $this->moduleList = [];

        if (file_exists($vendor)) {
            $jsonData = file_get_contents($vendor);
            if ($jsonData !== false) {
                $packages = json_decode($jsonData, true);

                foreach ($packages as $package) {
                    if (isset($package['source']['url'])) {
                        $src = $package['source']['url'];
                    } else {
                        $src = '-';
                    }

                    if (isset($package['keywords'])) {
                        $keywords = implode(",", $package['keywords']);
                    } else {
                        $keywords = '-';
                    }

                    $this->moduleList[] = [
                        $src,
                        strtolower($package['name']),
                        $package['version'],
                        $package['type'],
                        $keywords
                    ];
                }
            }
        }

        return $this->moduleList;
    }
}
