<?php


namespace Jarvis\Command\General;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ConfirmationQuestion;
use Symfony\Component\Console\Style\SymfonyStyle;

use Jarvis\Util\General\Update;

class GreetCommand extends Command
{
    /**
     * @var string
     */
    protected static $defaultName = 'general:greet';

    protected function configure(): void
    {
        $this->setHidden(true);
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $code = 0;
        $io = new SymfonyStyle($input, $output);
        $io->write([
            "\n",
            '<info>',
            '       __  ___      ____ _    __  ____ _____' . "\n",
            '      / / /   |    / __ \ |  / / /  _// ___/' . "\n",
            ' __  / / / /| |   / /_/ / | / /  / /  \__ \ ' . "\n",
            '/ /_/ / / ___ |_ / _, _/| |/ / _/ /_ ___/ / ' . "\n",
            '\____(_)_/  |_(_)_/ |_(_)___(_)___(_)____(_)' . "\n",
            '</info>',
            "\n",
        ]);

        $updateHelper = new Update();
        // check back compatibility with old versions
        $updateHelper->generateDefaultFolders();
        $wasUpdated = false;
        $app = $this->getApplication();
        if ($app !== null) {
            if ($updateHelper->shouldCheckForNewerVersion()) {
                $command = $app->find('general:self-update');

                $returnCode = $command->run($input, $output);
                if ($returnCode === 2) {
                    $wasUpdated = true;
                }
            }

            if (!$wasUpdated) {
                $command = $app->find('list');

                $arguments = [
                    'command' => 'list',
                ];

                $input = new ArrayInput($arguments);
                $code = $command->run($input, $output);
            }
        }

        return $code;
    }
}
