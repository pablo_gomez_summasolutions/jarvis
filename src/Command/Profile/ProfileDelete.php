<?php

namespace Jarvis\Command\Profile;

use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Command\Command;

use Jarvis\Command\Profile\Profile;

class ProfileDelete extends Command
{
    /**
     * @var string
     */
    protected static $defaultName = 'profile:delete';

    protected function configure(): void
    {
        $this->setDescription('Delete Profile')
            ->addArgument(
                'profile',
                InputArgument::REQUIRED,
                'Delete Profile by name'
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $code = 0;
        $profile = new Profile();
        $profile->checkProfileFile();
        $profileName = $input->getArgument('profile');
        $profileName = is_string($profileName) ? $profileName : '';

        $result = $profile->deleteProfileByName($profileName);

        if ($result) {
            $output->writeln(sprintf('<info>profile delete successfully  "%s"...</info>', $profileName));
        } else {
            $output->writeln(sprintf('<error>something was wrong deletting prifile "%s"...</error>', $profileName));
            $code = 1;
        }

        return $code;
    }
}
