<?php

namespace Jarvis\Command\Vault;

use GuzzleHttp\Psr7\Request;
use Jarvis\Traits\VaultTrait;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;

class ResetPassword extends Command
{
    use VaultTrait;

    /**
     * @var string
     */
    protected static $defaultName = 'vault:reset-password';

    protected function configure(): void
    {
        $this->setDescription('Resets current user password');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $code = 0;
        $user = $this->getVaultCurrentUser();
        if (!empty($user) && !empty($user['meta']['username'])) {
            $helper = $this->getHelper('question');
            $question = new Question('<comment>Please enter the new password: </comment>');
            $question->setHidden(true);
            $question->setHiddenFallback(false);
            $newPassword = $helper->ask($input, $output, $question);

            $question = new Question('<comment>Please confirm the new password: </comment>');
            $question->setHidden(true);
            $question->setHiddenFallback(false);
            $newPasswordConfirmation = $helper->ask($input, $output, $question);

            if ($newPassword !== $newPasswordConfirmation) {
                $output->writeln('<error>Password don\'t match.</error>');
                return -1;
            }

            $client = $this->getVaultClient();
            $path = '/auth/userpass/users/' . $user['meta']['username'];
            $completePath = $client->buildPath($path);
            $options = [
                'json' => [
                    'password' => $newPassword,
                ],
            ];
            $request = new Request('POST', $completePath);
            $response = $client->getResponseBuilder()->build($client->send($request, $options));
            $output->writeln('<info>Password updated successfully!!</info>');
            $filePath = $_SERVER['HOME'] . '/.bash_profile';
            $bashProfileUpdated = false;
            if (file_exists($filePath)) {
                $bashProfileContent = (string) file_get_contents($filePath);
                $pattern = '/VAULT_PASSWORD="(.*)"/';
                $replace = 'VAULT_PASSWORD="' . $newPassword . '"';
                $newContent = preg_replace($pattern, $replace, $bashProfileContent);
                if ($bashProfileContent != $newContent) {
                    file_put_contents($filePath, $newContent);
                    $bashProfileUpdated = true;
                }
            }

            if ($bashProfileUpdated) {
                $output->writeln("
<info>The bash profile ${filePath} was updated successfully with the new password.</info>
                ");
            } else {
                $output->writeln("
<error>The bash profile file was not updated, please update the 'VAULT_PASSWORD' environment variable.</error>
");
                $code = 1;
            }
        }

        return $code;
    }
}
