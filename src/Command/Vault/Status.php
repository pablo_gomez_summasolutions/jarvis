<?php

namespace Jarvis\Command\Vault;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Status extends Command
{
    /**
     * @var string
     */
    protected static $defaultName = 'vault:status';

    protected function configure(): void
    {
        $this->setDescription('Status access to vault server');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $code = 0;
        if (!$this->getVaultStatus()) {
            $output->writeln(sprintf(
                '<error>Environment VAULT_ADDR not found. Is Vault installed?</error>'
            ));
            $code = 1;
        } else {
            $output->writeln(sprintf('<info>you are logged successful</info>'));
        }

        return $code;
    }

    /**
     * @return bool
     */
    public static function getVaultStatus()
    {
        $vaultServer = getenv('VAULT_ADDR') ? getEnv('VAULT_ADDR') : "";
        $vaultToken = getenv('VAULT_TOKEN') ? getEnv('VAULT_TOKEN') : "";

        if (!$vaultServer && !$vaultToken) {
            return false;
        }

        return true;
    }
}
