<?php

namespace Jarvis\Command\MagentoCloud;

use Jarvis\Traits\MagentoCloudTrait;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use Symfony\Component\Console\Style\SymfonyStyle;

use Symfony\Component\Dotenv\Dotenv;

class EnvironmentMigrate extends Command
{
    use MagentoCloudTrait;

    /**
     * @var string
     */
    protected static $defaultName = 'magento-cloud:environment-migrate';

    protected function configure(): void
    {
        $this->setDescription('Gives the command to migrate DB and static files from one environment to another')
            ->addArgument(
                'source',
                InputArgument::REQUIRED,
                'The environment you are getting the information from'
            )
            ->addArgument(
                'destination',
                InputArgument::REQUIRED,
                'The environment you are getting the information to'
            )
            ->addOption(
                'strip',
                's',
                InputOption::VALUE_OPTIONAL,
                'Strip data for dev purposes',
                true
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $code = 0;
        $io = new SymfonyStyle($input, $output);

        $io->title('Project information');
        $io->text('Loading WARP configuration...');
        $dotenvFilePath = getcwd() . '/.env';
        if (!file_exists($dotenvFilePath)) {
            $io->text(
                sprintf(
                    '<error>Environment file not found at "%s". Is WARP installed?</error>',
                    $dotenvFilePath
                )
            );
        }

        $dotenv = new Dotenv();
        $dotenv->load($dotenvFilePath);
        $io->text(sprintf('<info>Loaded env file "%s"</info>', $dotenvFilePath));

        $io->newLine();

        $sourceEnv = $input->getArgument('source');
        $sourceEnv = is_string($sourceEnv) ? $sourceEnv : '';

        $destEnv = $input->getArgument('destination');
        $destEnv = is_string($destEnv) ? $destEnv : '';

        $io->text(sprintf('Loading project mysql credentials for <info>%s</info>', $sourceEnv));
        $sourceDbInfo = $this->getDbInfo($sourceEnv);
        $io->text('<info>Loaded mysql credentials info</info>');

        $io->newLine();

        $dumpPath = sprintf('/tmp/%s/%s_no-definer.sql.gz', $sourceDbInfo['path'], date('Y-m-d') . '_' . $sourceEnv);
        $io->text(sprintf('Dumping <info>%s</info> db into <info>%s</info>...', $sourceEnv, $dumpPath));
        $result = $this->dumpDb($sourceDbInfo, $dumpPath, $sourceEnv);
        $io->text(sprintf('Dump process completed!'));

        $io->newLine();

        $io->text(sprintf('Loading project mysql credentials for <info>%s</info>', $destEnv));
        $destDbInfo = $this->getDbInfo($destEnv);
        $io->text('<info>Loaded mysql credentials info</info>');

        $io->newLine();

        $io->text(sprintf('Backing up tables in <info>%s</info>', $destEnv));
        $tables = [
            'msp_tfa_user_config',
            'msp_tfa_trusted',
            'admin_user',
            'admin_passwords',
            'core_config_data'
        ];
        foreach ($tables as $table) {
            $io->text(sprintf('Backing up table <info>%s</info>...', $table));
            $tableDumpPath = sprintf(
                '/tmp/%s/%s.sql.gz',
                $destDbInfo['path'],
                date('Y-m-d') . '_' . $table
            );
            $result = $this->backupTable($destDbInfo, $table, $tableDumpPath, $destEnv);
            $io->text(sprintf('Backing up table <info>%s</info> completed', $table));
        }

        $io->newLine();

        $io->text(sprintf('Loading project mysql credentials for <info>%s</info>', $destEnv));
        $destDbInfo = $this->getDbInfo($destEnv);
        $io->text('<info>Loaded mysql credentials info</info>');

        $io->newLine();

        $backupDumpPath = sprintf(
            '/tmp/%s/%s_no-definer.sql.gz',
            $destDbInfo['path'],
            date('Y-m-d') . '_' . $destEnv
        );
        $io->text(sprintf('Backing up <info>%s</info> db into <info>%s</info>...', $destEnv, $backupDumpPath));
        $result = $this->dumpDb($destDbInfo, $backupDumpPath, $destEnv);
        $io->text(sprintf('Backup process completed!'));

        $io->newLine();

        $io->text(sprintf('Moving DB dump into <info>%s</info>...', $destEnv));
        $result = $this->rsyncFiles($sourceEnv, $destEnv, $dumpPath, sprintf('/tmp/%s/', $destDbInfo['path']));
        $io->text('Db Dump imported completed');

        $io->newLine();

        $restoreDumpPath = sprintf(
            '/tmp/%s/%s_no-definer.sql.gz',
            $destDbInfo['path'],
            date('Y-m-d') . '_' . $sourceEnv
        );
        $io->text(sprintf('Importing DB dump into <info>%s</info>...', $destEnv));
        $warningMsg = 'Please don\'t stop this process or it might result in data corruption';
        $io->text(sprintf('<error>%s</error>', $warningMsg));
        $result = $this->restoreDbDump($destDbInfo, $restoreDumpPath, $destEnv);
        $io->text('Db Dump imported completed');

        $io->newLine();

        $io->text(sprintf('Restoring tables in <info>%s</info>', $destEnv));
        foreach ($tables as $table) {
            $io->text(sprintf('Restoring table <info>%s</info>...', $table));
            $tableDumpPath = sprintf(
                '/tmp/%s/%s.sql.gz',
                $destDbInfo['path'],
                date('Y-m-d') . '_' . $table
            );
            $result = $this->restoreDbDump($destDbInfo, $tableDumpPath, $destEnv);
            $io->text(sprintf('Table <info>%s</info> restored', $table));
        }

        $io->newLine();

        $io->text(sprintf('Sanitizing <info>%s</info> database', $destEnv));
        $result = $this->sanitizeDb($destEnv);
        $io->text('<info>Db Dump imported completed</info>');

        $io->text(
            sprintf(
                'Synchronizing media from <info>%s</info> into <info>%s</info>...',
                $sourceEnv,
                $destEnv
            )
        );
        $result = $this->rsyncFiles($sourceEnv, $destEnv, 'pub/media/', 'pub/media/');
        $io->text('Media folder synchronization completed!');

        $io->newLine();

        return $code;
    }
}
