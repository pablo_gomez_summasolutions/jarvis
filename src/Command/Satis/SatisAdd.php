<?php

namespace Jarvis\Command\Satis;

use Jarvis\Traits\VaultTrait;
use Jarvis\Command\Satis\Satis as ConstantSatis;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class SatisAdd extends Command
{
    use VaultTrait;

    /**
     * @var string
     */
    protected static $defaultName = 'satis:add';

    const ARGUMENT_REPOSITORY = "repository";
    const ARRAY_COLUMN = "url";
    const ARRAY_KEY = "repositories";

    protected function configure(): void
    {
        $this->setDescription('Add repository to satis')
            ->addArgument(
                self::ARGUMENT_REPOSITORY,
                InputArgument::REQUIRED,
                'Repository Url'
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $client = ConstantSatis::SATIS_VAULT_CLIENT;
        $project = ConstantSatis::SATIS_VAULT_PROJECT;
        $environment = ConstantSatis::SATIS_VAULT_ENVIRONMENT;
        $server = ConstantSatis::SATIS_SERVER;

        $host = $this->getVaultHostInfo($client, $project, $environment, $server);

        $output->writeln(sprintf('<info>Access to satis %s to build</info>', $server));
        $code = 0;
        $result = $this->sshSatisAdd($host, $input, $output);
        if (!$result) {
            $code = 1;
        }

        return $code;
    }

    /**
     * @param mixed $host
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return bool
     */
    public function sshSatisAdd($host, $input, $output): bool
    {
        $satisJson = ConstantSatis::SATIS_JSON_PATH;
        $tmpDirectory = "/tmp/" . md5(ConstantSatis::SATIS_JSON_NAME);
        $repository = $input->getArgument(self::ARGUMENT_REPOSITORY);
        if (!is_string($repository)) {
            return false;
        }

        if (!$this->validateRepository($repository)) {
            $output->writeln(sprintf('<error>Error validating the repository %s"</error>', $repository));
            $output->writeln(sprintf('<error>format valid "git@github.com:user/project.git"</error>'));
        } else {
            $cmd = (string)sprintf(
                "scp -i %s %s@%s:%s %s",
                $host['pem_file'],
                $host['username'],
                $host['ip'],
                $satisJson,
                $tmpDirectory
            );

            $process = proc_open($cmd, [STDIN, STDOUT, STDERR], $pipes);
            if ($process !== false) {
                proc_close($process);
            }

            $contents = (string)file_get_contents($tmpDirectory);
            $contents = utf8_encode($contents);
            $tmpJson = json_decode($contents, true);
            unlink($tmpDirectory);

            try {
                $repositories_columns = array_column($tmpJson[self::ARRAY_KEY], self::ARRAY_COLUMN);
                $search_repository = array_search($repository, $repositories_columns);
            } catch (\Exception $e) {
                $output->writeln(sprintf('<error>Error reading data from %s</error>', ConstantSatis::SATIS_JSON_NAME));

                return false;
            }

            if ($search_repository) {
                $output->writeln(sprintf('<error>Repository %s already exists within satis json</error>', $repository));
            } else {
                $dataRepository = [
                    "type" => "vcs",
                    "url" => $repository
                ];

                array_push($tmpJson[self::ARRAY_KEY], $dataRepository);
                $jsonData = json_encode($tmpJson, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT);
                file_put_contents($tmpDirectory, $jsonData);

                $cmd = (string)sprintf(
                    "scp -i %s %s %s@%s:%s",
                    $host['pem_file'],
                    $tmpDirectory,
                    $host['username'],
                    $host['ip'],
                    $satisJson . ".temp"
                );

                proc_open($cmd, [STDIN, STDOUT, STDERR], $pipes);

                $cmd = (string)sprintf(
                    "scp -i %s %s %s@%s:%s",
                    $host['pem_file'],
                    $tmpDirectory,
                    $host['username'],
                    $host['ip'],
                    $satisJson
                );

                $process = proc_open($cmd, [STDIN, STDOUT, STDERR], $pipes);

                if ($process !== false) {
                    proc_close($process);
                }

                unlink($tmpDirectory);
            }
        }
        return true;
    }

    protected function validateRepository(string $repository): bool
    {
        $rest = substr($repository, -4);
        $start = substr($repository, 0, 4);

        if ($start === "git@" && $rest === ".git") {
            return true;
        } else {
            return false;
        }
    }
}
