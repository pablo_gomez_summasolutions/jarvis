<?php

namespace Jarvis\Command\DevOps;

use Symfony\Component\Console\Command\Command;
use Jarvis\Util\Console\Helper\ScanDir;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

use Symfony\Component\Console\Input\InputOption;
use Jarvis\Util\Console\Helper\Table\Renderer\RendererFactory;
use Jarvis\Util\Console\Helper\TableHelper;

class Terraform extends Command
{
    /**
     * @var string
     */
    protected static $defaultName = 'devops:terraform';

    protected function configure(): void
    {
        $this->setDescription('Info DevOps Modules of Terraform')
            ->addOption(
                'format',
                null,
                InputOption::VALUE_OPTIONAL,
                'Output Format. One of [' . implode(',', RendererFactory::getFormats()) . ']'
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $code = 0;
        $tf = (string) getcwd();
        $tf_modules = $this->getTFModules($tf);

        $format = $input->getOption('format');
        $format = is_string($format) ? $format : '';

        /* @var $tableHelper TableHelper */
        $tableHelper = new TableHelper($output);
        $tableHelper
            ->setHeaders(['src', 'name', 'version', 'type'])
            ->renderByFormat($output, $tf_modules, $format);

        return $code;
    }

    /**
     * @param string $tf
     * @return mixed
     */
    public function getTFModules($tf)
    {
        $tfState = [];

        $files = ScanDir::scan($tf, "tf");

        $needle = "git";

        foreach ($files as $file) {
            $content = file_get_contents($file);
            if ($content !== false) {
                preg_match_all("|([^\n]+.*{$needle}.*)|", $content, $out, PREG_PATTERN_ORDER);

                $cron_log_array = $out[0];
                foreach ($cron_log_array as $log_entry) {
                    $separated = $this->parseTfSource(trim($log_entry));

                    $tfState[] = [$separated[1], rtrim($separated[2], '/'), $separated[3], 'terraform'];
                }
            }
        }

        return $tfState;
    }

    /**
     * @param string $string
     * @return mixed
     */
    private function parseTfSource(string $string)
    {
        $regEx = '/source[\s\S]+git::ssh:\/\/(.*)\/\/(.*)\?ref=(.*)\"/';
        preg_match($regEx, $string, $matches);

        return $matches;
    }

    /**
     * @param string $delimiters
     * @param string $string
     * @return mixed
     */
    public function getMultiExplode(string $delimiters, string $string)
    {

        $ready = str_replace($delimiters, $delimiters[0], $string);
        $launch = explode($delimiters[0], $ready);
        return $launch;
    }
}
