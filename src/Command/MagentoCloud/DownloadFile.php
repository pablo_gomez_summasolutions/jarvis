<?php

namespace Jarvis\Command\MagentoCloud;

use Jarvis\Traits\MagentoCloudTrait;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Dotenv\Dotenv;

class DownloadFile extends Command
{
    use MagentoCloudTrait;

    /**
     * @var string
     */
    protected static $defaultName = 'magento-cloud:download-file';

    protected function configure(): void
    {
        $this->setDescription('Downloads a file from a remote server of Magento Cloud')
            ->addArgument('environment', InputArgument::REQUIRED, 'The environment you want to download the file from')
            ->addArgument('file', InputArgument::REQUIRED, 'The file you want to download');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $code = 0;
        $this->io = new SymfonyStyle($input, $output);

        $this->io->title('Project information');
        $this->io->text('Loading WARP configuration...');
        $dotenvFilePath = getcwd() . '/.env';
        if (!file_exists($dotenvFilePath)) {
            $this->io->text(
                sprintf(
                    '<error>Environment file not found at "%s". Is WARP installed?</error>',
                    $dotenvFilePath
                )
            );
        }

        $dotenv = new Dotenv();
        $dotenv->load($dotenvFilePath);
        $this->io->text(sprintf('<info>Loaded env file "%s"</info>', $dotenvFilePath));

        $this->io->newLine();

        $env = $input->getArgument('environment');
        if (empty($env) || !is_string($env)) {
            $this->io->text(sprintf('<error>Environment is not valid</error>'));
            return 1;
        }

        $file = $input->getArgument('file');
        $file = is_string($file) ? $file : '';

        $this->io->text(sprintf('Downloading file <info>%s</info>...', $file));
        $this->downloadFiles($env, $file);

        return $code;
    }
}
