<?php

namespace Jarvis\Command\Satis;

use Jarvis\Traits\VaultTrait;
use Jarvis\Command\Satis\Satis as ConstantSatis;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class SatisBuild extends Command
{
    use VaultTrait;

    /**
     * @var string
     */
    protected static $defaultName = 'satis:build';

    protected function configure(): void
    {
        $this->setDescription('Build Satis');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $code = 0;
        $client      = ConstantSatis::SATIS_VAULT_CLIENT;
        $project     = ConstantSatis::SATIS_VAULT_PROJECT;
        $environment = ConstantSatis::SATIS_VAULT_ENVIRONMENT;
        $server      = ConstantSatis::SATIS_SERVER;

        $host = $this->getVaultHostInfo($client, $project, $environment, $server);

        $output->writeln(sprintf('<info>Access to satis %s to build</info>', $server));
        $this->sshSatisBuild($host);

        return $code;
    }

    /**
     * @param mixed $host
     * @return int|null
     */
    public function sshSatisBuild($host)
    {

        $satisBin    = ConstantSatis::SATIS_BIN_PATH;
        $satisJson   = ConstantSatis::SATIS_JSON_PATH;
        $satisOutput = ConstantSatis::SATIS_OUTPUT_PATH;

        $cmd = (string)sprintf(
            "ssh -tt -i %s %s@%s %s build %s %s",
            $host['pem_file'],
            $host['username'],
            $host['ip'],
            $satisBin,
            $satisJson,
            $satisOutput
        );

        $result = null;

        $process = proc_open($cmd, [STDIN, STDOUT, STDERR], $pipes);
        if ($process !== false) {
            $result = proc_close($process);
        }

        return $result;
    }
}
