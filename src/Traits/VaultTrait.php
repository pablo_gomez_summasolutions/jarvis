<?php

namespace Jarvis\Traits;

use Vault\AuthenticationStrategies\TokenAuthenticationStrategy;
use Vault\Client;
use VaultTransports\Guzzle6Transport;
use GuzzleHttp\Psr7\Request;
use Jarvis\Traits\LocalStorageTrait;

trait VaultTrait
{
    use LocalStorageTrait;

    /**
     * @var Client
     */
    protected $vaultClient;

    /**
     * @return Client
     * @throws \Exception
     */
    public function getVaultClient()
    {
        if (empty($this->vaultClient)) {
            $vaultServer = getEnv('VAULT_ADDR');
            $vaultToken = (string)getEnv('VAULT_TOKEN');

            $client = new Client(new Guzzle6Transport(['base_uri' => $vaultServer]));

            $authenticated = $client
                ->setAuthenticationStrategy(new TokenAuthenticationStrategy($vaultToken))
                ->authenticate();

            if (!$authenticated) {
                throw new \Exception('You are not logged in Vault');
            }

            $this->vaultClient = $client;
        }

        return $this->vaultClient;
    }

    /**
     * @param string $path
     * @return mixed[]
     * @throws \Vault\Exceptions\TransportException
     * @throws \Vault\Exceptions\ServerException
     * @throws \Vault\Exceptions\ClientException
     */
    public function getVaultSecret($path)
    {
        $client = $this->getVaultClient();

        $response = $client->read($path);

        $data = $response->getData(); // Raw array with secret's content.

//        $data = json_encode($data, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);

        return $data;
    }

    /**
     * @param string $path
     * @return mixed[]
     * @throws \Vault\Exceptions\TransportException
     * @throws \Vault\Exceptions\ServerException
     * @throws \Vault\Exceptions\ClientException
     */
    public function getVaultList($path)
    {
        $client = $this->getVaultClient();

        $completePath = $client->buildPath($path);
        $options = [];
        $request = new Request('LIST', $completePath);
        $response = $client->getResponseBuilder()->build($client->send($request, $options));

        $data = $response->getData(); // Raw array with secret's content.

//        $data = json_encode($data, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);

        return $data;
    }

    /**
     * @param string $client
     * @param string $project
     * @param string $environment
     * @param string $server
     * @param null|string|mixed $proxy
     * @param bool $cache
     * @return mixed[]
     */
    public function getVaultHostInfo($client, $project, $environment, $server, $proxy = null, $cache = false)
    {
        $secretPath = "/secret/{$client}/{$project}/{$environment}-{$server}-ssh";

        $secretValue = $this->getVaultSecret($secretPath);

        if (!empty($secretValue['key_path'])) {
            $localStorageKey = "{$client}/{$project}/{$environment}/{$secretValue['key_path']}";
            $pemPath = $this->getLocalStoragePath($localStorageKey);

            if (!empty($pemPath) && !empty($secretValue['is_on_demand']) || $cache === true) {
                $this->deleteLocalStorage($localStorageKey);
                $pemPath = '';
            }

            if (empty($pemPath) && !empty($secretValue['key_path'])) {
                $serverKeyPath = "/secret/{$client}/{$project}/{$secretValue['key_path']}";
                $serverKeyValue = $this->getVaultSecret($serverKeyPath);
                $pemPath = $this->saveLocalStorage(
                    $localStorageKey,
                    $serverKeyValue['private'],
                    0400,
                    1
                );
            }
        }

        $host = [
            'ip' => $secretValue['ip'],
            'username' => $secretValue['username'],
            'password' => !empty($secretValue['password']) ? $secretValue['password'] : '',
            'pem_file' => !empty($pemPath) ? $pemPath : null,
        ];

        $proxyPath = $proxy;
        if (empty($proxyPath) || !is_string($proxyPath)) {
            $proxyPath = !empty($secretValue['proxy_path']) ? (string)$secretValue['proxy_path'] : '';
        } else {
            $proxyPath = "{$environment}-{$proxyPath}-ssh";
        }
        if (!empty($proxyPath)) {
            $secretPath = "/secret/{$client}/{$project}/{$proxyPath}";
            $jumphostSecretValue = $this->getVaultSecret($secretPath);

            $localStorageKey = "{$client}/{$project}/{$environment}/{$jumphostSecretValue['key_path']}";
            $jumphostPemPath = $this->getLocalStoragePath($localStorageKey);
            if (empty($jumphostPemPath) && !empty($secretValue['key_path'])) {
                $serverKeyPath = "/secret/{$client}/{$project}/{$jumphostSecretValue['key_path']}";
                $serverKeyValue = $this->getVaultSecret($serverKeyPath);

                $jumphostPemPath = $this->saveLocalStorage(
                    $localStorageKey,
                    $serverKeyValue['private'],
                    0400,
                    1
                );
            }

            $host['proxy'] = [
                'ip' => $jumphostSecretValue['ip'],
                'username' => $jumphostSecretValue['username'],
                'pem_file' => $jumphostPemPath,
            ];
        }

        return $host;
    }

    /**
     * @param string $client
     * @param string $project
     * @param string $environment
     * @param string $server
     * @param null|mixed $proxy
     * @return mixed[]
     */
    public function getVaultMySQLInfo($client, $project, $environment, $server, $proxy = null)
    {
        $secretPath = "/secret/${client}/${project}/${environment}-${server}-mysql";
        $secretValue = $this->getVaultSecret($secretPath);

        $mysql["mysql"]["user"] = $secretValue['username'];
        $mysql["mysql"]["pass"] = $secretValue['password'];
        $mysql["mysql"]["host"] = str_replace(":3306", "", $secretValue['host']);
        $mysql["mysql"]["database"] = $secretValue['database'];

        $ssh = [];
        $proxyPath = $proxy;
        if (empty($proxyPath) || !is_string($proxyPath)) {
            if (!empty($secretValue['executor_path'])) {
                $proxyPath = (string)$secretValue['executor_path'];
                $mysql["mysql"]["executor_path"] = $proxyPath;
            } else {
                $proxyPath = '';
            }
        } else {
            $proxyPath = "{$environment}-{$proxyPath}-ssh";
        }
        if (!empty($proxyPath)) {
            $executorInfo = explode('-', $proxyPath);
            $executorType = array_pop($executorInfo);
            $executorEnv = (string) array_shift($executorInfo);
            $executorLabel = implode('-', $executorInfo);

            $ssh = $this->getVaultHostInfo($client, $project, $executorEnv, $executorLabel, $proxy);
        }

        return array_merge($ssh, $mysql);
    }

    /**
     * @return mixed[]
     * @throws \Exception
     */
    public function getVaultCurrentUser()
    {
        $client = $this->getVaultClient();
        $path = '/auth/token/lookup-self';
        // Request exception could appear here.
        /** @varq \Vault\ResponseModels\Response $response */
        $response = $client->read($path);

        $data = $response->getData(); // Raw array with secret's content.

//        $data = json_encode($data, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);

        return $data;
    }
}
