![J.A.R.V.I.S](img/logo.png)

# J.A.R.V.I.S.

A php cli tool to make the developer's life a little bit less painful.

## Philosophy
The main idea behind this tools is to avoid wasting time or having to memorize repetitive tasks. 
It sits on top of other tools and uses as much information of the context as possible to avoid asking for parameters that can be inferred.

## Pre-requisites

### WARP
Your project has to be setup with the latest version of [WARP](http://ct.summasolutions.net/warp-engine/).

### Magento Cloud Cli Tool
If you are going to use the cloud commands provided with this tool you need to have the official cli tool provide by Magento, check [here](https://devdocs.magento.com/guides/v2.3/cloud/before/before-workspace-magento-prereqs.html#cloud-ssh-cli-cli-install) for installing instructions.

