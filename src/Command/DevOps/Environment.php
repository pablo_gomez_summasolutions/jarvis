<?php

namespace Jarvis\Command\DevOps;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Dotenv\Dotenv;
use Symfony\Component\Yaml\Yaml;
use Symfony\Component\Yaml\Exception\ParseException;

use Symfony\Component\Console\Input\InputOption;
use Jarvis\Util\Console\Helper\Table\Renderer\RendererFactory;
use Jarvis\Util\Console\Helper\TableHelper;

class Environment extends Command
{
    public const ENV_SELLERCENTER = 'sellercenter';
    public const ENV_MAGENTO = 'm2';
    public const ENV_ORO = 'oro';

    /**
     * @var string
     */
    protected static $defaultName = 'devops:environment';

    /**
     * @var mixed
     */
    public $ciConfig;

    public function __construct(string $name = null)
    {
        $this->ciConfig = $this->ciConfig();
        parent::__construct($name);
    }

    protected function configure(): void
    {
        $this->setDescription('Info DevOps swg_ci configuration')
            ->addOption(
                'format',
                null,
                InputOption::VALUE_OPTIONAL,
                'Output Format. One of [' . implode(',', RendererFactory::getFormats()) . ']'
            )
            ->addOption(
                'environment',
                '-e',
                InputOption::VALUE_OPTIONAL,
                'Environment'
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $code = 0;
        $this->ciConfig = $this->ciConfig();
        $env = $input->getOption('environment');

         $parsed[] = [
             "client" => $this->getClient($env),
             "project" => $this->getProject(),
             "framework" => $this->getFramework()
         ];

         $format = $input->getOption('format');
         $format = is_string($format) ? $format : '';

        /* @var $tableHelper TableHelper */
         $tableHelper = new TableHelper($output);
         $tableHelper
            ->setHeaders(['client', 'project', 'framework'])
            ->renderByFormat($output, $parsed, $format);

         return $code;
    }

    /**
     * @return mixed
     */
    public function ciConfig()
    {
        $yamlPath = getcwd() . "/swg_ci.yml";
        $dotEnvFilePath = getcwd() . '/.env.sample';
        $config = [];

        try {
            if (file_exists($dotEnvFilePath)) {
                $data = (string)file_get_contents($dotEnvFilePath);
                $dotEnv = new Dotenv();
                $config = array_change_key_case($dotEnv->parse($data), CASE_LOWER);
            } elseif (file_exists($yamlPath)) {
                $config = Yaml::parseFile($yamlPath);
            }
        } catch (ParseException $exception) {
            printf('Unable to parse the YAML or Env: %s', $exception->getMessage());
        }

        return $config;
    }

    /**
     * @param mixed $env
     * @return string
     */
    public function getClient($env = null)
    {
        $ArrayOptions = ['client', 'NAMESPACE'];
        $ArrayOptions =  array_map('strtolower', $ArrayOptions);

        $config = $this->ciConfig;
        if (isset($env)) {
            return (isset($config['deploy'][$env])) ? $config['deploy'][$env]['client'] : '';
        } else {
            return $this->arrayKeysExists($config, $ArrayOptions) ?
                (isset($config['client']) ? $config['client']: $config[strtolower('NAMESPACE')]) : '';
        }
    }

    public function getProject(): string
    {
        $config = $this->ciConfig;
        return isset($config['project']) ? $config['project'] : '';
    }

    public function getFramework(): string
    {
        $ArrayOptions = ['framework', 'platform'];
        $config = $this->ciConfig;
        return $this->arrayKeysExists($config, $ArrayOptions) ?
            (isset($config['framework']) ? $config['framework']: $config['platform']) : '';
    }

    /**
     * @param mixed $array
     * @param mixed $keys
     * @return bool
     */
    public function arrayKeysExists($array, $keys): bool
    {
        foreach ($keys as $k) {
            if (isset($array[$k])) {
                return true;
            }
        }

        return false;
    }
}
