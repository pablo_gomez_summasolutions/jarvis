<?php

namespace Jarvis\Util\General;

class Shell
{
    public const OS_LINUX = 'linux';
    public const OS_OSX = 'osx';

    /**
     * @var string[]
     */
    public static $sshPassInstructions = [
        'osx' => 'brew install https://raw.githubusercontent.com/kadwanev/bigboybrew/master/Library/Formula/sshpass.rb',
        'linux' => "yum install sshpass #For CentOS, RedHat or Amazon Linux\n
apt-get install sshpass #For Debian, Ubuntu",
    ];

    public function getOs(): string
    {
        return php_uname('s') === 'Darwin' ? self::OS_OSX : self::OS_LINUX;
    }

    public function checkCmdExists(string $cmd): bool
    {
        $return = shell_exec(sprintf("which %s", escapeshellarg($cmd)));
        return !empty($return);
    }

    public function validateSshPass(): void
    {
        if (!$this->checkCmdExists('sshpass')) {
            $os = $this->getOs();
            $instructions = self::$sshPassInstructions[$os];
            throw new \Exception("You don't  have \"sshpass\" installed, please follow this 
            instructions to install it: \n\n" . $instructions);
        }
    }
}
