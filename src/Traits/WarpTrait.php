<?php
declare(strict_types=1);

namespace Jarvis\Traits;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Dotenv\Dotenv;

trait WarpTrait
{
    /**
     * @var mixed[]
     */
    protected $warpConfig;

    public function loadWarpConfig(InputInterface $input, OutputInterface $output): void
    {
        $io = new SymfonyStyle($input, $output);

        if ($output->isVerbose()) {
            $io->text('Loading WARP configuration...');
        }
        $dotenvFilePath = getcwd() . '/.env';
        if (!file_exists($dotenvFilePath)) {
            $io->getErrorStyle()->text(
                sprintf(
                    '<error>Environment file not found at "%s". Is WARP installed?</error>',
                    $dotenvFilePath
                )
            );
        }

        $dotenv = new Dotenv();
        $dotenv->load($dotenvFilePath);

        if ($output->isVerbose()) {
            $io->text(sprintf('<info>Configuration file loaded: %s</info>', $dotenvFilePath));
        }
    }

    /**
     * @return mixed[]
     */
    public function getWarpConfig()
    {
        if (empty($this->warpConfig)) {
            $dotenvFilePath = getcwd() . '/.env.sample';
            if (file_exists($dotenvFilePath)) {
                $data = (string)file_get_contents($dotenvFilePath);
                $dotenv = new Dotenv();
                $this->warpConfig = $dotenv->parse($data);
            }
        }

        return $this->warpConfig;
    }

    public function getWarpVersion(): string
    {
        $versionFile = getcwd() . '/.warp/lib/version.sh';
        $warpBin = getcwd() . '/warp';
        if (!file_exists($versionFile) && file_exists($warpBin)) {
            shell_exec("warp update --force");
        }

        $warpVersion = '';
        if (file_exists($versionFile)) {
            $cmd = ". {$versionFile} && echo \$WARP_VERSION";
            $warpVersion = (string)shell_exec($cmd);
            $warpVersion = str_replace("\n", "", $warpVersion);
        }

        return $warpVersion;
    }

    public function getWarpClient(): string
    {
        $config = $this->getWarpConfig();

        return isset($config['NAMESPACE']) ? $config['NAMESPACE'] : '';
    }

    public function getWarpProject(): string
    {
        $config = $this->getWarpConfig();

        return isset($config['PROJECT']) ? $config['PROJECT'] : '';
    }
}
