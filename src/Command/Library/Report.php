<?php

namespace Jarvis\Command\Library;

use Jarvis\Command\Platform\InfoMagento;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\HttpClient\HttpClient;

use Jarvis\Command\DevOps\Ansible;
use Jarvis\Command\DevOps\Terraform;
use Jarvis\Command\DevOps\Composer;
use Jarvis\Command\DevOps\Environment;

use Jarvis\Traits\TerraformTrait;
use Jarvis\Traits\VaultTrait;
use Jarvis\Traits\WarpTrait;
use Jarvis\Traits\JenkinsLibraryTrait;

class Report extends Command
{

    use TerraformTrait;
    use VaultTrait;
    use WarpTrait;
    use JenkinsLibraryTrait;

    /**
     * @var string
     */
    protected static $defaultName = 'library:report';

    protected function configure(): void
    {
        $this->setDescription('Report to Library')
            ->addArgument(
                'url',
                InputArgument::REQUIRED,
                'API'
            )
            ->addArgument(
                'token',
                InputArgument::REQUIRED,
                'API Token'
            )
            ->addArgument(
                'repository',
                InputArgument::REQUIRED,
                'Repository'
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $code = 0;
        $fullName = $input->getArgument('repository');

        $environment = new Environment();
        $framework = $environment->getFramework();

        switch ($framework) {
            case Environment::ENV_MAGENTO:
                $report = [
                    "id" => $fullName,
                    "client" => $this->getClient(),
                    "project" => $this->getProject(),
                    "platform" => $this->getMagentoInfo(),
                    "composer" => $this->getComposerModules(),
                    "terraform" => $this->getTerraformModules(),
                    "ansible" => $this->getAnsibleModules(),
                    "tools" => $this->getToolModules(),
                ];
                break;
            case Environment::ENV_ORO:
            case Environment::ENV_SELLERCENTER:
            default:
                $report = [
                    "id" => $fullName,
                    "client" => $environment->getClient(),
                    "project" => $environment->getProject(),
                    "framework" => $environment->getFramework()
                ];
        }

        $url = $input->getArgument('url');
        $url = is_string($url) ? $url : '';

        $token = $input->getArgument('token');
        $token = is_string($token) ? $token : '';

        /**
         * @var \Symfony\Contracts\HttpClient\HttpClientInterface $client
         */
        $client = HttpClient::create(['http_version' => '2.0']);

        $response = $client->request('POST', $url, [
            'headers' => [
                'Content-Type' => 'application/json',
                'X-API-KEY' => $token,
            ],
            'body' => json_encode($report, JSON_FORCE_OBJECT | JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT),
        ]);

        $output->writeln(sprintf('<info>%s</info>', $response->getStatusCode()));

        return $code;
    }

    public function getClient(): string
    {
        $return = $this->getTfClient();
        if (empty($return)) {
            $return = $this->getWarpClient();
        }

        return $return;
    }

    public function getProject(): string
    {
        $return = $this->getTfProject();
        if (empty($return)) {
            $return = $this->getWarpProject();
        }

        return $return;
    }

    /**
     * @return mixed
     */
    protected function getTerraformModules()
    {
        try {
            $tf = (string) getcwd();
            $terraform = new Terraform();
            $terraformOutput = $terraform->getTFModules($tf);
        } catch (\Exception $e) {
            $terraformOutput = [];
        }

        $header = array_values(['src', 'name', 'version', 'type']);
        $terraformOutput = $this->formatData($terraformOutput, $header);

        return $terraformOutput;
    }

    /**
     * @return mixed
     */
    protected function getMagentoInfo()
    {
        try {
            $magento = new InfoMagento();

            if ($magento->getMagentoApp()) {
                $magento->addVersionInfo();
                $magento->addDeploymentInfo();
                $magento->addCacheInfos();
                $magento->addVendors();
            }

            $dataMagento = $magento->getInfo();
        } catch (\Exception $e) {
            $dataMagento = [];
        }

        return $dataMagento;
    }

    /**
     * @return mixed
     */
    protected function getComposerModules()
    {
        $dataComposer = [];

        if (file_exists(getcwd() . '/vendor/composer/installed.json')) {
            $autoloadFilePath = getcwd() . '/vendor/composer/installed.json';

            $composer = new Composer();
            $dataComposer = $composer->prepareModuleList($autoloadFilePath);
        }

        $header = array_values(['src', 'name', 'version', 'type', 'keywords']);
        $dataComposer = $this->formatData($dataComposer, $header);

        return $dataComposer;
    }

    /**
     * @return array|mixed
     */
    protected function getAnsibleModules()
    {
        $dataAnsible = [];

        $yaml = '';
        if (file_exists(getcwd() . "/requirements.yml")) {
            $yaml = getcwd() . "/requirements.yml";
        } elseif (file_exists(getcwd() . "/../requirements.yml")) {
            $yaml = getcwd() . "/../requirements.yml";
        }

        if (!empty($yaml)) {
            $ansible = new Ansible();
            $dataAnsible = $ansible->getModulesDevOps($yaml);

            $header = array_values(['src', 'name', 'version', 'type']);
            $dataAnsible = $this->formatData($dataAnsible, $header);
        }

        return $dataAnsible;
    }

    /**
     * @return mixed
     */
    protected function getToolModules()
    {
        $tools = [];
        $warpVersion = $this->getWarpVersion();
        if (!empty($warpVersion)) {
            $tools[] = [
                'name' => 'warp',
                'version' => $warpVersion,
                'type' => 'tool',
                'src' => '-',
            ];
        }

        $jenkinsLibraryInfo = $this->getJenkinsLibraryInfo();
        if (!empty($jenkinsLibraryInfo)) {
            $tools[] = [
                'name' => 'swg-jenkins-library',
                'version' => $jenkinsLibraryInfo['version'],
                'type' => 'tool',
                'src' => '-',
            ];
        }

        return $tools;
    }

    /**
     * @param mixed $data
     * @param mixed $headers
     * @return mixed
     */
    protected function formatData($data, $headers)
    {
        foreach ($data as &$row) {
            $row = array_combine($headers, $row);
        }

        return $data;
    }
}
