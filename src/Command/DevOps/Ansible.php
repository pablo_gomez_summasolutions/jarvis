<?php

namespace Jarvis\Command\DevOps;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Yaml\Yaml;
use Symfony\Component\Yaml\Exception\ParseException;

use Symfony\Component\Console\Input\InputOption;
use Jarvis\Util\Console\Helper\Table\Renderer\RendererFactory;
use Jarvis\Util\Console\Helper\TableHelper;

class Ansible extends Command
{
    /**
     * @var string
     */
    protected static $defaultName = 'devops:ansible';

    protected function configure(): void
    {
        $this->setDescription('Info DevOps Modules of Ansible')
            ->addOption(
                'format',
                null,
                InputOption::VALUE_OPTIONAL,
                'Output Format. One of [' . implode(',', RendererFactory::getFormats()) . ']'
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $code = 0;
        $yaml = getcwd() . "/requirements.yml";

        if (!file_exists($yaml)) {
            $output->writeln(sprintf(
                '<error>Ansible file not found at "%s".</error>',
                $yaml
            ));

            return 1;
        }

        $parsed = $this->getModulesDevOps($yaml);

        $format = $input->getOption('format');
        $format = is_string($format) ? $format : '';

        /* @var $tableHelper TableHelper */
        $tableHelper = new TableHelper($output);
        $tableHelper
            ->setHeaders(['src', 'name', 'version', 'type'])
            ->renderByFormat($output, $parsed, $format);

        return $code;
    }

    /**
     * @param string $yaml
     * @return mixed
     */
    public function getModulesDevOps(string $yaml)
    {

        $modules = [];

        try {
            $parsed = Yaml::parseFile($yaml);

            foreach ($parsed as $item) {
                $src = str_replace('git+', '', $item['src']);
                $parts = explode(':', $src);
                $name = $item['name'];
                if (sizeof($parts) >= 2) {
                    $name = str_replace('.git', '', $parts[1]);
                }
                $modules[] = [$src, $name, $item['version'], 'ansible'];
            }
        } catch (ParseException $exception) {
            printf('Unable to parse the YAML string: %s', $exception->getMessage());
        }

        return $modules;
    }
}
