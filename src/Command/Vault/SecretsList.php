<?php

namespace Jarvis\Command\Vault;

use Jarvis\Util\Console\Helper\Table\Renderer\RendererFactory;
use Jarvis\Util\Console\Helper\TableHelper;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class SecretsList extends BaseVaultCommand
{
    /**
     * @var string
     */
    protected static $defaultName = 'vault:secrets';

    protected function configure(): void
    {
        $this->setDescription('List all secrets for a particular client, project and environment')
            ->addOption(
                'format',
                null,
                InputOption::VALUE_OPTIONAL,
                'Output Format. One of [' . implode(',', RendererFactory::getFormats()) . ']'
            );

        $this->getCommandOptions($this);
    }

    protected function executeCommand(InputInterface $input, OutputInterface $output): int
    {
        $code = 0;
        $path = "/secret/{$this->currentClient}/{$this->currentProject}";

        $secretsList = $this->getVaultList($path);
        $parsedSecretsList = $this->parseSecretsList($secretsList['keys']);

        $format = $input->getOption('format');
        if (!is_string($format)) {
            $format = '';
        }


        /* @var $tableHelper TableHelper */
        $tableHelper = new TableHelper($output);
        $tableHelper
            ->setHeaders(['label', 'type'])
            ->renderByFormat($output, $parsedSecretsList, $format);

        return $code;
    }

    /**
     * @param string[] $secrets
     * @return mixed
     */
    protected function parseSecretsList($secrets)
    {
        $list = [];
        foreach ($secrets as $secret) {
            $parts = explode('-', $secret);
            if (sizeof($parts) >= 3) {
                if ($parts[0] == $this->currentEnvironment) {
                    $type = array_pop($parts);
                    $environment = array_shift($parts);
                    $label = implode('-', $parts);

                    $list[] = [
                        'label' => $label,
                        'type' => $type,
                    ];
                }
            }
        }

        return $list;
    }
}
