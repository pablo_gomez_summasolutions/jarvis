<?php


namespace Jarvis\Command\General;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ConfirmationQuestion;
use Symfony\Component\Console\Style\SymfonyStyle;

use Jarvis\Util\General\Update;

class SelfUpdate extends Command
{
    /**
     * @var string
     */
    protected static $defaultName = 'general:self-update';

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $updateHelper = new Update();
        $io->writeln('Checking for newer versions...');

        $app = $this->getApplication();
        $currentVersion = $app !== null ? $app->getVersion() : '';
        $latestVersion = $updateHelper->getLatestVersion();

        if ($updateHelper->isNewVersionAvailable($currentVersion)) {
            $helper = $this->getHelper('question');
            $io->write([
                '<bg=blue;options=blink>There is a newer version!!</>' . "\n",
                sprintf('Your current version is <info>%s</info>', $currentVersion) . "\n",
                sprintf('The latest version is <info>%s</info>', $latestVersion) . "\n",
            ]);
            $question = new ConfirmationQuestion(
                '<question>Do you want to update to the latest version?</question> [Y/n]',
                true
            );
            $shouldUpdate = $helper->ask($input, $output, $question);
            if ($shouldUpdate) {
                $updateHelper->downloadLatestVersion($io);
                return 2;
            }
        } else {
            $io->write([
                '<bg=cyan;fg=black>You are running in the latest version!!</>' . "\n",
            ]);
        }

        return 0;
    }
}
