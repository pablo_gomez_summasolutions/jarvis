<?php

namespace Jarvis\Util\General;

use Jarvis\Traits\LocalStorageTrait;
use phpDocumentor\Reflection\Types\Boolean;
use Symfony\Component\HttpClient\HttpClient;

class Update
{
    use LocalStorageTrait;

    public const CHECK_FOR_UPDATES_FREQUENCY = 7;
    public const CURRENT_VERSION_URL = 'https://bitbucket.org/summasolutions/jarvis/downloads/version';
    public const LATEST_JARVIS_URL = 'https://bitbucket.org/summasolutions/jarvis/downloads/jarvis.phar';

    public function generateDefaultFolders(): void
    {
        $this->checkDefaultFolders();
    }

    public function shouldCheckForNewerVersion(): bool
    {
        $fileKey = 'update-last-checked';
        $lastTimeChecked = $this->getLocalStorage($fileKey);
        $return = true;
        $now = (string) time(); // or your date as well
        if (empty($lastTimeChecked)) {
            $daysDiff = self::CHECK_FOR_UPDATES_FREQUENCY;
        } else {
            $dateDiff = $now - (int)$lastTimeChecked;
            $daysDiff = round($dateDiff / (60 * 60 * 24));
        }

        if ($daysDiff >= self::CHECK_FOR_UPDATES_FREQUENCY) {
            $this->saveLocalStorage($fileKey, $now, null, 0);
        } else {
            $return = false;
        }

        return $return;
    }

    public function getLatestVersion(): string
    {
        $client = HttpClient::create();
        $response = $client->request('GET', self::CURRENT_VERSION_URL);
        $return = '';
        if ($response->getStatusCode() == 200) {
            $return = trim($response->getContent());
        }

        return $return;
    }

    public function isNewVersionAvailable(string $currentVersion): bool
    {
        $latestVersion = $this->getLatestVersion();
        $latestVersionParts = explode('_', $latestVersion);
        $latestVersionNumber = (int)$latestVersionParts[1];

        $currentVersionParts = explode('_', $currentVersion);
        $currentVersionNumber = (int)(!empty($currentVersionParts[1]) ? $currentVersionParts[1] : 0);

        return (bool)($latestVersionNumber > $currentVersionNumber);
    }

    public function downloadLatestVersion(\Symfony\Component\Console\Style\SymfonyStyle $io): void
    {
        $client = HttpClient::create();
        $io->writeln('Downloading...');
        $response = $client->request('GET', self::LATEST_JARVIS_URL);
        $fileName = $this->saveLocalStorage('jarvis-executable', $response->getContent(), 0755, 0);
        if (substr(php_uname(), 0, 6) == "Darwin") {
            shell_exec("mv {$fileName} /usr/local/bin/jarvis");
        } else {
            shell_exec("sudo mv {$fileName} /usr/local/bin/jarvis");
        }
        $io->writeln('<info>J.A.R.V.I.S was successfully updated!!</info>');
    }
}
