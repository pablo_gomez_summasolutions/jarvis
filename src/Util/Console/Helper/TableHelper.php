<?php

namespace Jarvis\Util\Console\Helper;

use Jarvis\Util\Console\Helper\Table\Renderer\RendererFactory;
use Jarvis\Util\Console\Helper\Table\Renderer\RendererInterface;
use Symfony\Component\Console\Helper\Table as BaseTableHelper;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Text Table Helper
 * @author Timothy Anido <xanido@gmail.com>
 *
 * Based on draw_text_table by Paul Maunders
 * Available at http://www.pyrosoft.co.uk/blog/2007/07/01/php-array-to-text-table-function/
 */
class TableHelper extends BaseTableHelper
{
    /**
     * @var string
     */
    protected $format;

    /**
     * @var string[]
     */
    protected $headers = array();

    /**
     * @param string $format
     * @return $this
     */
    public function setFormat($format)
    {
        $this->format = $format;

        return $this;
    }

    /**
     * @return string
     */
    public function getFormat()
    {
        return $this->format;
    }

    /**
     * @param string[] $headers
     * @return $this
     */
    public function setHeaders(array $headers)
    {
        $this->headers = array_values($headers);
        parent::setHeaders($headers);

        return $this;
    }

    /**
     * @param OutputInterface $outputInterface
     * @param array[] $rows
     * @param string $format [optional]
     */
    public function renderByFormat(OutputInterface $outputInterface, array $rows, $format = null): void
    {
        $rendererFactory = new RendererFactory();
        $renderer = $rendererFactory->create($format);
        if ($renderer && $renderer instanceof RendererInterface) {
            foreach ($rows as &$row) {
                $row = array_combine($this->headers, $row);
            }
            $renderer->render($outputInterface, $rows);
        } else {
            $this->setRows($rows);
            parent::render();
        }
    }
}
