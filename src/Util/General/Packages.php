<?php

namespace Jarvis\Util\General;

class Packages
{
    const ARGUMENT_CUSTOMER = "customer";
    const ARGUMENT_MODULE   = "module";

    const VAULT_SECRET_PATH = "/secret/summa/dev/packages";
    const VAULT_USERNAME    = "server_user";
    const VAULT_IP_ADDRESS  = "server_host";
}
