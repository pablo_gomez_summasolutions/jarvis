<?php

namespace Jarvis\Command\Vault;

use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class Scp extends BaseVaultCommand
{
    /**
     * @var string
     */
    protected static $defaultName = 'vault:scp';

    protected function configure(): void
    {
        $this->setDescription('Allows to copy files between local and remote servers')
            ->setHelp("This commands allows you to copy files from a remote server to your local machine 
or upload a file from your local machine to a server.

The way to pass the source and destination parameters is inspired in <info>scp</info> command.
So all <comment>local</comment> paths are referred directly, a few examples are:

- <info>~/file.txt</info>
- <info>/var/www/</info>

On the other hand, for all <comment>remote</comment> paths the pattern used needs to add the label of the server,
following this pattern:
<info>server</info>:<info>path</info>

A few example of this are:
- <info>jumphost:~/keys/internal</info>
- <info>app-0:/data/www/</info>

------------------------------------------------------
A few examples of how this is used.

- Download a sql file from the <info>master</info> server: 
  <info>jarvis vault:scp master:~/dump.sql ~/</info>

- Upload a php file to the <info>app-0</info> server: 
  <info>jarvis vault:scp /path/to/script.php app-0:/data/www/</info>
            ")
            ->addArgument(
                'source',
                InputArgument::REQUIRED,
                'Source file(s)'
            )
            ->addArgument(
                'destination',
                InputArgument::REQUIRED,
                'Destination folder'
            );

        $this->getCommandOptions($this);
    }

    protected function executeCommand(InputInterface $input, OutputInterface $output): int
    {
//        $output->writeln(sprintf('<info>Connecting to server "%s"...</info>', $server));
        $code = 0;
        $source = $input->getArgument('source');
        $parsedSource = $this->parseParameter($source);

        $destination = $input->getArgument('destination');
        $parsedDestination = $this->parseParameter($destination);


        $this->scpExec($parsedSource, $parsedDestination);

        return $code;
    }

    /**
     * @param mixed $value
     * @return mixed
     */
    public function parseParameter($value)
    {
        $client = $this->currentClient;
        $project = $this->currentProject;
        $environment = $this->currentEnvironment;

        $parts = explode(':', $value);
        $parsedValue = [];
        if (sizeof($parts) == 2) {
            $parsedValue = [
                'host' => $this->getVaultHostInfo($client, $project, $environment, $parts[0]),
                'path' => $parts[1],
            ];
        } else {
            $parsedValue = [
                'path' => $parts[0],
            ];
        }

        return $parsedValue;
    }

    /**
     * @param mixed $source
     * @param mixed $destination
     * @return int|null
     */
    public function scpExec($source, $destination)
    {
        $pemFile = '';
        $proxyStr = '';
        $sourceStr = $source['path'];
        $destinationStr = $destination['path'];

        if (!empty($source['host'])) {
            $pemFile = '-i ' . $source['host']['pem_file'];
            $sourceStr = sprintf(
                "%s@%s:%s",
                $source['host']['username'],
                $source['host']['ip'],
                $source['path']
            );
            if (!empty($source['host']['proxy'])) {
                $proxyStr = sprintf(
                    "-o ProxyCommand='ssh -i %s %s@%s nc %s 22'",
                    $source['host']['proxy']['pem_file'],
                    $source['host']['proxy']['username'],
                    $source['host']['proxy']['ip'],
                    $source['host']['ip']
                );
            }
        }


        if (!empty($destination['host'])) {
            $pemFile = '-i ' . $destination['host']['pem_file'];
            $destinationStr = sprintf(
                "%s@%s:%s",
                $destination['host']['username'],
                $destination['host']['ip'],
                $destination['path']
            );
            if (!empty($destination['host']['proxy'])) {
                $proxyStr = sprintf(
                    "-o ProxyCommand='ssh -i %s %s@%s nc %s 22'",
                    $destination['host']['proxy']['pem_file'],
                    $destination['host']['proxy']['username'],
                    $destination['host']['proxy']['ip'],
                    $destination['host']['ip']
                );
            }
        }
        $cmd = (string)sprintf(
            "scp %s %s %s %s",
            $pemFile,
            $proxyStr,
            $sourceStr,
            $destinationStr
        );

        $result = null;

        $process = proc_open($cmd, [STDIN, STDOUT, STDERR], $pipes);
        if ($process !== false) {
            $result = proc_close($process);
        }

        return $result;
    }
}
