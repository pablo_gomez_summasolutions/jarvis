<?php


namespace Jarvis\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Exception\LogicException;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

abstract class AbstractCommand extends Command
{
    public function execute(InputInterface $input, OutputInterface $output): int
    {
        //$this->loadWarpConfig($input, $output);

        return $this->executeCommand($input, $output);
    }

    protected function executeCommand(InputInterface $input, OutputInterface $output): int
    {
        $output->writeln('<error>You must override the execute() method in the concrete command class.</error>');

        return 1;
    }
}
