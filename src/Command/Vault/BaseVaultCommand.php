<?php

namespace Jarvis\Command\Vault;

use Jarvis\Command\Profile\Profile;
use Jarvis\Traits\TerraformTrait;
use Jarvis\Traits\VaultTrait;
use Jarvis\Traits\WarpTrait;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;

abstract class BaseVaultCommand extends Command
{
    use TerraformTrait;
    use VaultTrait;
    use WarpTrait;

    /**
     * @var string
     */
    protected $currentClient;

    /**
     * @var string
     */
    protected $currentProject;

    /**
     * @var string
     */
    protected $currentEnvironment;

    /**
     * @var string[]
     */
    protected $currentProfile;

    public const OPTION_CLIENT = 'client';
    public const OPTION_PROJECT = 'project';
    public const OPTION_ENVIRONMENT = 'environment';
    public const OPTION_NO_CACHE = 'no-cache';

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $helper = $this->getHelper('question');

        $profile = $input->getOption('profile');
        if (!empty($profile) && is_string($profile)) {
            $loadProfile = new Profile;
            $this->currentProfile = $loadProfile->getProfileByName($profile);
        }

        $clientOptionValue = $input->getOption(self::OPTION_CLIENT);
        if (!empty($clientOptionValue) && is_string($clientOptionValue)) {
            $this->currentClient = $clientOptionValue;
        } else {
            $this->currentClient = $this->getClient();
        }

        if (empty($this->currentClient)) {
            $question = new Question('<comment>Please enter the name of the Client: </comment>');
            $this->currentClient = $helper->ask($input, $output, $question);
        }
        $output->writeln(sprintf('<info>The Client is "%s"</info>', $this->currentClient));

        $projectOptionValue = $input->getOption(self::OPTION_PROJECT);
        if (!empty($projectOptionValue) && is_string($projectOptionValue)) {
            $this->currentProject = $projectOptionValue;
        } else {
            $this->currentProject = $this->getProject();
        }

        if (empty($this->currentProject)) {
            $question = new Question('<comment>Please enter the name of the Project: </comment>');
            $this->currentProject = $helper->ask($input, $output, $question);
        }
        $output->writeln(sprintf('<info>The Project is "%s"</info>', $this->currentProject));

        $environmentOptionValue = $input->getOption(self::OPTION_ENVIRONMENT);
        if (!empty($environmentOptionValue) && is_string($environmentOptionValue)) {
            $this->currentEnvironment = $environmentOptionValue;
        } else {
            $this->currentEnvironment = $this->getEnvironment();
        }

        if (empty($this->currentEnvironment)) {
            $question = new Question('<comment>Please enter the name of the Environment: </comment>');
            $this->currentEnvironment = $helper->ask($input, $output, $question);
        }
        $output->writeln(sprintf('<info>The Environment is "%s"</info>', $this->currentEnvironment));


        $code = $this->executeCommand($input, $output);

        return $code;
    }

    abstract protected function executeCommand(InputInterface $input, OutputInterface $output): int;

    /**
     * @return mixed|string|null
     */
    public function getClient()
    {
        if (isset($this->currentProfile[Profile::OPTION_CLIENT])) {
            return $this->currentProfile[Profile::OPTION_CLIENT];
        }

        $return = $this->getTfClient();
        if (empty($return)) {
            $return = $this->getWarpClient();
        }

        return $return;
    }

    /**
     * @return mixed|string|null
     */
    public function getProject()
    {
        if (isset($this->currentProfile[Profile::OPTION_PROJECT])) {
            return $this->currentProfile[Profile::OPTION_PROJECT];
        }

        $return = $this->getTfProject();
        if (empty($return)) {
            $return = $this->getWarpProject();
        }

        return $return;
    }

    /**
     * @return mixed|string|null
     */
    public function getEnvironment()
    {
        if (isset($this->currentProfile[Profile::OPTION_ENVIRONMENT])) {
            return $this->currentProfile[Profile::OPTION_ENVIRONMENT];
        }

        return $this->getTfEnvironment();
    }

    public function getCommandOptions(Command $command): Command
    {
        $command->addOption(
            'profile',
            null,
            InputOption::VALUE_OPTIONAL,
            'Load custom profile'
        )
            ->addOption(
                self::OPTION_CLIENT,
                null,
                InputOption::VALUE_OPTIONAL,
                'Client'
            )
            ->addOption(
                self::OPTION_PROJECT,
                null,
                InputOption::VALUE_OPTIONAL,
                'Project'
            )
            ->addOption(
                self::OPTION_ENVIRONMENT,
                null,
                InputOption::VALUE_OPTIONAL,
                'Environment'
            )
            ->addOption(
                self::OPTION_NO_CACHE,
                null,
                InputOption::VALUE_NONE,
                'Renews certificates, avoid using cache. It also updates the cache for future uses'
            );

        return $command;
    }

    /**
     * @param string $client
     * @param string $project
     * @param string $environment
     * @param string $type
     * @return mixed
     */
    public function getServerLists($client, $project, $environment, $type)
    {
        $serverList = [];
        $path = "/secret/{$client}/{$project}";
        $secretsList = $this->getVaultList($path);

        foreach ($secretsList['keys'] as $secret) {
            $parts = explode('-', $secret);
            if (sizeof($parts) >= 3) {
                if ($parts[0] == $environment) {
                    $_type = array_pop($parts);
                    if ($_type == $type) {
                        $_environment = array_shift($parts);
                        $_label = implode('-', $parts);
                        $serverList[] = $_label;
                    }
                }
            }
        }

        return $serverList;
    }
}
