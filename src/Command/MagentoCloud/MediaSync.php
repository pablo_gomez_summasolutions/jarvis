<?php

namespace Jarvis\Command\MagentoCloud;

use Jarvis\Traits\MagentoCloudTrait;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use Symfony\Component\Console\Style\SymfonyStyle;

use Symfony\Component\Dotenv\Dotenv;

class MediaSync extends Command
{
    use MagentoCloudTrait;

    /**
     * @var string
     */
    protected static $defaultName = 'magento-cloud:media-sync';

    protected function configure(): void
    {
        $this->setDescription('Synchronizes media between environments')
            ->addArgument(
                'source',
                InputArgument::REQUIRED,
                'The environment you are getting the media from'
            )
            ->addArgument(
                'destination',
                InputArgument::REQUIRED,
                'The environment you are putting the media to'
            )
            ->addOption(
                'exclude',
                'e',
                InputOption::VALUE_OPTIONAL,
                'Folders to be excluded'
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $code = 0;
        $io = new SymfonyStyle($input, $output);
        $this->io = $io;

        $io->title('Project information');
        $io->text('Loading WARP configuration...');
        $dotenvFilePath = getcwd() . '/.env';
        if (!file_exists($dotenvFilePath)) {
            $io->text(
                sprintf(
                    '<error>Environment file not found at "%s". Is WARP installed?</error>',
                    $dotenvFilePath
                )
            );
        }

        $dotenv = new Dotenv();
        $dotenv->load($dotenvFilePath);
        $io->text(sprintf('<info>Loaded env file "%s"</info>', $dotenvFilePath));

        $io->newLine();

        $sourceEnv = $input->getArgument('source');
        $sourceEnv = is_string($sourceEnv) ? $sourceEnv : '';

        $destEnv = $input->getArgument('destination');
        $destEnv = is_string($destEnv) ? $destEnv : '';


        $excludes = ['/catalog/product/cache*'];
        $customExcludes = $input->getOption('exclude');
        $customExcludes = is_string($customExcludes) ? $customExcludes : '';
        if (!empty($customExcludes)) {
            $customExcludes = explode(',', $customExcludes);
            $excludes = array_merge($excludes, $customExcludes);
        }


        $io->text(sprintf('Synchronizing media from <info>%s</info> into <info>%s</info>...', $sourceEnv, $destEnv));
        $this->rsyncFiles($sourceEnv, $destEnv, 'pub/media/', 'pub/media/', $excludes);
        $io->text('Media folder synchronization completed!');

        $io->newLine();

        return $code;
    }
}
