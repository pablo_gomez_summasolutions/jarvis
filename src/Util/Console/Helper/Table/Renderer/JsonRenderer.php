<?php

namespace Jarvis\Util\Console\Helper\Table\Renderer;

use Symfony\Component\Console\Output\OutputInterface;

class JsonRenderer implements RendererInterface
{
    /**
     * @param OutputInterface $output
     * @param array[]           $rows
     */
    public function render(OutputInterface $output, array $rows): void
    {
        $options = JSON_FORCE_OBJECT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES;
        if (version_compare(PHP_VERSION, '5.4', '>=')) {
            $options |= JSON_PRETTY_PRINT;
        }

        $data = json_encode($rows, $options);
        if (is_string($data)) {
            $output->writeln($data);
        }
    }
}
