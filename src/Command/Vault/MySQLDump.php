<?php

namespace Jarvis\Command\Vault;

use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ChoiceQuestion;

class MySQLDump extends BaseVaultCommand
{
    /**
     * @var string
     */
    protected static $defaultName = 'vault:dump:mysql';

    protected function configure(): void
    {
        $this->setDescription('Access to server and generates a DB dump')
            ->addArgument(
                'server',
                InputArgument::OPTIONAL,
                'The server to connect to'
            )
            ->addArgument(
                'proxy',
                InputArgument::OPTIONAL,
                'The proxy server to be used to connect to the server. 
                Check vault:secrets for the list of servers'
            );

        $this->getCommandOptions($this);
    }

    protected function executeCommand(InputInterface $input, OutputInterface $output): int
    {
        $code = 0;
        $client = $this->currentClient;
        $project = $this->currentProject;
        $environment = $this->currentEnvironment;
        $proxy = $input->getArgument('proxy');

        $server = $input->getArgument('server');
        if (empty($server) || !is_string($server)) {
            try {
                $servers = $this->getServerLists($client, $project, $environment, 'mysql');
                if (count($servers) == 1) {
                    $server = $servers[0];
                } else {
                    $helper = $this->getHelper('question');
                    $question = new ChoiceQuestion(
                        'Please select a server (default to ' . $servers[0] . ')',
                        $servers,
                        0
                    );
                    $question->setErrorMessage('Option %s is invalid.');
                    $server = $helper->ask($input, $output, $question);
                }
            } catch (\Exception $e) {
                $output->writeln('server is not defined, use default server');
                $server = 'db';
                $code =1;
            }
        }

        $output->writeln(sprintf('<info>Connecting to mysql in server "%s"...</info>', $server));

        $host  = $this->getVaultMySQLInfo($client, $project, $environment, $server, $proxy);
        $this->sshLogin($host);

        return $code;
    }

    /**
     * @param mixed $host
     * @return int|null
     * @throws \Exception
     */
    public function sshLogin($host)
    {
        $mysqlCmd = (string)sprintf(
            "mysqldump -u%s -p%s -h%s %s",
            $host['mysql']['user'],
            $host['mysql']['pass'],
            $host['mysql']['host'],
            $host['mysql']['database']
        );

        if (!empty($host["mysql"]["executor_path"])) {
            $cmd = new Ssh();
            $command = (string)sprintf(
                "%s -tt \"%s\"",
                $cmd->getSshPrompt($host),
                $mysqlCmd
            );
        } else {
            $command = $mysqlCmd;
        }
        $result = null;

        $process = proc_open($command, [STDIN, STDOUT, STDERR], $pipes);
        if ($process !== false) {
            $result = proc_close($process);
        }

        return $result;
    }
}
