<?php

namespace Jarvis\Command\Satis;

class Satis
{
    const SATIS_BIN_PATH    = "/var/www/satis/bin/satis";
    const SATIS_OUTPUT_PATH = "/var/www/satis/web/";
    const SATIS_JSON_PATH   = "/var/www/satis/satis.json";
    const SATIS_JSON_NAME   = "satis.json";

    const SATIS_VAULT_CLIENT        = "summa";
    const SATIS_VAULT_PROJECT       = "tools";
    const SATIS_VAULT_ENVIRONMENT   = "ops";
    const SATIS_SERVER              = "packages";
}
