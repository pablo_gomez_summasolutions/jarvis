<?php

namespace Jarvis\Command\MagentoCloud;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;

use Symfony\Component\Console\Style\SymfonyStyle;

use Symfony\Component\Dotenv\Dotenv;

class Tmux extends Command
{
    /**
     * @var string
     */
    protected static $defaultName = 'magento-cloud:tmux';

    protected function configure(): void
    {
        $this->setDescription('Opens TMUX screens with all the server for a given environment')
            ->addArgument('environment', InputArgument::REQUIRED, 'The environment you want to connect to');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $code = 0;
        $io = new SymfonyStyle($input, $output);

        $io->title('Project information');
        $io->text('Loading WARP configuration...');
        $dotenvFilePath = getcwd() . '/.env';
        if (!file_exists($dotenvFilePath)) {
            $io->text(
                sprintf(
                    '<error>Environment file not found at "%s". Is WARP installed?</error>',
                    $dotenvFilePath
                )
            );
        }

        $dotenv = new Dotenv();
        $dotenv->load($dotenvFilePath);
        $io->text(sprintf('<info>Loaded env file "%s"</info>', $dotenvFilePath));

        $io->newLine();

        $env = $input->getArgument('environment');
        $env = is_string($env) ? $env : '';

        $io->text(sprintf('Getting servers information for <info>%s</info>', $env));
        $sshEndpoints = $this->getSshInfo($env);

        $io->text(sprintf('Opening TMUX session for <info>%s</info>', $env));
        $return = $this->openTmuxSession($sshEndpoints);
        $io->text($return);
        $io->newLine();

        return $code;
    }

    /**
     * @param string $env
     * @return mixed
     */
    private function getSshInfo(string $env)
    {
        $process = new Process([
            'magento-cloud',
            'ssh',
            '--all',
            '--environment=' . $env
        ]);
        $process->run();

        if (!$process->isSuccessful()) {
            throw new ProcessFailedException($process);
        }

        $rawInfo = $process->getOutput();

        return explode(PHP_EOL, $rawInfo);
    }

    /**
     * @param mixed $sshEndpoints
     * @return mixed
     */
    private function openTmuxSession($sshEndpoints)
    {
        $isFirst = true;
        $rawInfo = [];
        foreach ($sshEndpoints as $sshEndpoint) {
            if (empty($sshEndpoint)) {
                continue;
            }

            if ($isFirst) {
                $isFirst = false;
                $cmd = [
                    'tmux',
                    'new-window',
                    'ssh ' . $sshEndpoint
                ];
                $process = new Process($cmd);
                $process->start();

                foreach ($process as $type => $data) {
                    if ($process::OUT === $type) {
                        echo "\nRead from stdout: " . $data;
                    } else { // $process::ERR === $type
                        echo "\nRead from stderr: " . $data;
                    }
                }
                $rawInfo[] = $process->getOutput();
            } else {
                $cmd = [
                    'tmux',
                    'split-window',
                    '-h',
                    'ssh ' . $sshEndpoint
                ];
                $process = new Process($cmd);
                $process->run();

                if (!$process->isSuccessful()) {
                    throw new ProcessFailedException($process);
                }

                $rawInfo[] = $process->getOutput();


                $process = new Process([
                    'tmux',
                    'select-layout',
                    'tiled'
                ]);
                $process->run();

                if (!$process->isSuccessful()) {
                    throw new ProcessFailedException($process);
                }

                $rawInfo[] = $process->getOutput();
            }
        }

        $process = new Process([
            'tmux',
            'select-pane',
            '-t',
            '0'
        ]);
        $process->run();

        $rawInfo[] = $process->getOutput();
        $process = new Process([
            'tmux',
            'set-window-option',
            'synchronize-panes',
            'on'
        ]);
        $process->run();

        $rawInfo[] = $process->getOutput();
        return implode(PHP_EOL, $rawInfo);
    }
}
