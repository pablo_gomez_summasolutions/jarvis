<?php

namespace Jarvis\Util\Console\Helper\Table\Renderer;

class RendererFactory
{
    /**
     * @var string[]
     */
    protected static $formats = [
        'csv'  => 'Jarvis\Util\Console\Helper\Table\Renderer\CsvRenderer',
        'json' => 'Jarvis\Util\Console\Helper\Table\Renderer\JsonRenderer',
        'xml'  => 'Jarvis\Util\Console\Helper\Table\Renderer\XmlRenderer',
    ];

    /**
     * @param string|null $format
     *
     * @return bool|RendererInterface
     */
    public function create($format)
    {
        $format = strtolower((string) $format);
        if (isset(self::$formats[$format])) {
            $rendererClass = self::$formats[$format];
            return new $rendererClass;
        }

        return false;
    }

    /**
     * @return string[]
     */
    public static function getFormats(): array
    {
        return array_keys(self::$formats);
    }
}
