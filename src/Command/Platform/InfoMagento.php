<?php

namespace Jarvis\Command\Platform;

use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Command\Command;

use Jarvis\Util\Console\Helper\TableHelper;
use Jarvis\Util\Console\Helper\Table\Renderer\RendererFactory;

use Magento\Framework\App\Bootstrap;
use Magento\Framework\App\State as AppState;

class InfoMagento extends Command
{
    protected static $defaultName = 'platform:info:magento';

    /**
     * @var array
     */
    protected $infos = [];

    /**
     * @var \Magento\Framework\App\ProductMetadataInterface
     */
    protected $productMetadata;

    /**
     * @var \Magento\Customer\Model\CustomerFactory
     */
    protected $customerFactory;

    /**
     * @var \Magento\Catalog\Model\ProductFactory
     */
    protected $productFactory;

    /**
     * @var \Magento\Catalog\Model\CategoryFactory
     */
    protected $categoryFactory;

    /**
     * @var \Magento\Eav\Model\Entity\AttributeFactory
     */
    protected $attributeFactory;

    /**
     * @var \Magento\Framework\App\Cache\Type\FrontendPool
     */
    protected $frontendPool;

    /**
     * @var \Magento\Framework\Module\ModuleListInterface
     */
    protected $moduleList;

    /**
     * @var \Magento\Framework\App\DeploymentConfig
     */
    protected $deploymentConfig;

    /**
     * @var \Magento\Framework\Filesystem\DirectoryList $directoryList
     */
    protected $directoryList;

    protected function configure()
    {
        $this->setDescription('Info About Platform')
            ->addArgument(
                'key',
                InputArgument::OPTIONAL,
                'Only output value of named param like "version". Key is case insensitive.'
            )
            ->addOption(
                'format',
                null,
                InputOption::VALUE_OPTIONAL,
                'Output Format. One of [' . implode(',', RendererFactory::getFormats()) . ']'
            );
    }

    /**
     * Magento Application
     *
     * @return bool
     */
    public function getMagentoApp()
    {
        try {
            /* Find autoload path */
            $rootPath = getcwd();
            $autoload_file = $rootPath . '/app/autoload.php';

            if (!file_exists($autoload_file)) {
                fwrite(STDERR, "[ERROR] Unable to find autoload file \"$autoload_file\"\n");
            } else {
                /* Include Magento autoload file */
                require_once $autoload_file;

                $bootstrap = \Magento\Framework\App\Bootstrap::create(BP, $_SERVER);
                $obj = $bootstrap->getObjectManager();

                $this->productMetadata = $obj->get('\Magento\Framework\App\ProductMetadataInterface');
                $this->customerFactory = $obj->get('\Magento\Customer\Model\CustomerFactory');
                $this->productFactory = $obj->get('\Magento\Catalog\Model\ProductFactory');
                $this->categoryFactory = $obj->get('\Magento\Catalog\Model\CategoryFactory');
                $this->attributeFactory = $obj->get('\Magento\Eav\Model\Entity\AttributeFactory');
                $this->frontendPool = $obj->get('\Magento\Framework\App\Cache\Type\FrontendPool');
                $this->deploymentConfig = $obj->get('\Magento\Framework\App\DeploymentConfig');
                $this->moduleList = $obj->get('\Magento\Framework\Module\ModuleListInterface');
                $this->directoryList = $obj->get('\Magento\Framework\Filesystem\DirectoryList');

                return true;
            }

            return false;
        } catch (\Exception $e) {
            $e->getMessage();
            $e->getCode();
        }

        return false;
    }

    public function hasInfo()
    {
        return !empty($this->infos);
    }

    public function getInfo($key = null)
    {
        if (is_null($key)) {
            return $this->infos;
        }

        return isset($this->infos[$key]) ? $this->infos[$key] : null;
    }

    /**
     * @param \Symfony\Component\Console\Input\InputInterface   $input
     * @param \Symfony\Component\Console\Output\OutputInterface $output
     *
     * @return int|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $table = [];

        if ($this->getMagentoApp()) {
            $this->addVersionInfo();
            $this->addDeploymentInfo();
            $this->addCacheInfos();
            $this->addVendors();
            //$this->addAttributeCount();
            //$this->addCustomerCount();
            //$this->addCategoryCount();
            //$this->addProductCount();

            foreach ($this->infos as $key => $value) {
                $table[] = [$key, $value];
            }
        }

        if (($settingArgument = $input->getArgument('key')) !== null) {
            $settingArgument = strtolower($settingArgument);
            $this->infos = array_change_key_case($this->infos, CASE_LOWER);
            if (!isset($this->infos[$settingArgument])) {
                throw new \InvalidArgumentException('Unknown key: ' . $settingArgument);
            }
            $output->writeln((string)$this->infos[$settingArgument]);
        } else {
            $tableHelper = new TableHelper($output);
            $tableHelper
                ->setHeaders(['name', 'value'])
                ->renderByFormat($output, $table, $input->getOption('format'));
        }
    }

    /**
     * @todo there is also the product repository API...?!
     */
    public function addProductCount()
    {
        $this->infos['product_count'] = $this->productFactory
            ->create()
            ->getCollection()
            ->getSize();
    }

    public function addCustomerCount()
    {
        $this->infos['customer_count'] = $this->customerFactory
            ->create()
            ->getCollection()
            ->getSize();
    }

    public function addCategoryCount()
    {
        $this->infos['category_count'] = $this->categoryFactory
            ->create()
            ->getCollection()
            ->getSize();
    }

    public function addAttributeCount()
    {
        $this->infos['attribute_count'] = $this->attributeFactory
            ->create()
            ->getCollection()
            ->getSize();
    }

    public function addCacheInfos()
    {
        $cachePool = $this->frontendPool;

        $this->infos['cache_backend'] = get_class($cachePool->get('config')->getBackend());

        switch (get_class($cachePool->get('config')->getBackend())) {
            case 'Zend_Cache_Backend_File':
            case 'Cm_Cache_Backend_File':
                // @TODO Where are the cache options?
                //$cacheDir = $cachePool->get('config')->getBackend()->getOptions()->getCacheDir();
                //$this->infos['Cache Directory'] = $cacheDir;
                break;

            default:
        }
    }

    public function addDeploymentInfo()
    {
        $this->infos['application_mode'] = $this->deploymentConfig->get(AppState::PARAM_MODE);
        $this->infos['session'] = $this->deploymentConfig->get('session/save');
        //$this->infos['Crypt Key'] = $this->deploymentConfig->get('crypt/key');
        $this->infos['install_date'] = $this->deploymentConfig->get('install/date');
    }

    public function addVersionInfo()
    {
        $this->infos['name'] = $this->productMetadata->getName();
        $this->infos['code'] = strtolower($this->productMetadata->getName());
        $this->infos['version'] = $this->productMetadata->getVersion();
        $this->infos['edition'] = $this->productMetadata->getEdition();
        $this->infos['root'] = $this->directoryList->getRoot();
    }

    public function addVendors()
    {
        $vendors = [];

        $moduleList = $this->moduleList->getAll();

        foreach ($moduleList as $moduleName => $info) {
            // First index is (probably always) vendor
            $moduleNameData = explode('_', $moduleName);

            if (isset($moduleNameData[0])) {
                $vendors[] = $moduleNameData[0];
            }
        }

        if (!empty($vendors)) {
            $this->infos['vendors'] = implode(', ', array_unique($vendors));
        }
    }
}
