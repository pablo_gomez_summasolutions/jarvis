<?php

namespace Jarvis\Command\Magento;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class BackupMedia extends Command
{
    /**
     * @var string
     */
    protected static $defaultName = 'magento:backup-media';

    protected function configure(): void
    {
        $this->setDescription('Generates a TGZ backup of the media folder')
            ->addOption(
                'optimize',
                'o',
                InputOption::VALUE_NONE,
                'Exclude generated media content (cache, captcha, etc)'
            )
            ->addOption(
                'dry-run',
                'd',
                InputOption::VALUE_NONE,
                'Only display the command to be executed'
            )
            ->addOption(
                'text',
                't',
                InputOption::VALUE_NONE,
                'Output only text'
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $code = 0;
        $io = new SymfonyStyle($input, $output);
        $excludedFolders = [
            './pub/media/captcha',
            './pub/media/catalog/product/cache',
        ];
        $command = "tar ";
        $optimize = $input->getOption('optimize');

        if ($optimize) {
            foreach ($excludedFolders as $excludedFolder) {
                $command .= "--exclude='${excludedFolder}' ";
            }
        }

        $fileName = sprintf('media-%s.tgz', date('Y_m_d'));
        $command .= " -zcvf ${fileName} ./pub/media ";

        $dryRun = $input->getOption('dry-run');
        $outputText = $input->getOption('text');
        if ($dryRun) {
            if ($outputText) {
                $io->write($command);
            } else {
                $io->writeln('The command to be executed is:');
                $io->writeln(sprintf('<info>%s</info>', $command));
            }
        } else {
            //TODO: Implement execution
        }

        return $code;
    }
}
