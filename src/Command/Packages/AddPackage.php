<?php

namespace Jarvis\Command\Packages;

use Jarvis\Traits\VaultTrait;
use Jarvis\Util\General\Packages as ConstantPackages;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class AddPackage extends Command
{
    use VaultTrait;

    const ARGUMENT_CUSTOMER = ConstantPackages::ARGUMENT_CUSTOMER;
    const ARGUMENT_MODULE   = ConstantPackages::ARGUMENT_MODULE;
    const VAULT_SECRET_PATH = ConstantPackages::VAULT_SECRET_PATH;
    const VAULT_USERNAME    = ConstantPackages::VAULT_USERNAME;
    const VAULT_IP_ADDRESS  = ConstantPackages::VAULT_IP_ADDRESS;

    /**
     * @var string
     */
    protected static $defaultName = 'packages:add';

    protected function configure(): void
    {
        $this->setDescription('Add packages')
            ->addArgument(
                self::ARGUMENT_CUSTOMER,
                InputArgument::REQUIRED,
                'require module from private packagist'
            )
            ->addArgument(
                self::ARGUMENT_MODULE,
                InputArgument::REQUIRED,
                'require module from private packagist'
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $secrets    = $this->getVaultSecret(self::VAULT_SECRET_PATH);
        $username   = $secrets[self::VAULT_USERNAME];
        $ip         = $secrets[self::VAULT_IP_ADDRESS];

        $client = $input->getArgument(self::ARGUMENT_CUSTOMER);
        $client = is_string($client) ? $client : '';

        $module = $input->getArgument(self::ARGUMENT_MODULE);
        $module = is_string($module) ? $module : '';

        $output->writeln(sprintf('<info>Adding module %s in customer %s</info>', $module, $client));
        $code = $this->sshCommand($username, $ip, $client, $module);

        return $code;
    }

    protected function sshCommand(string $username, string $ip, string $customerName, string $module): int
    {
        $cmd = (string)sprintf(
            "ssh -tt %s@%s ./packages/utils/addpackage  %s %s",
            $username,
            $ip,
            $customerName,
            $module
        );

        $result = 0;

        $process = proc_open($cmd, [STDIN, STDOUT, STDERR], $pipes);
        if ($process !== false) {
            $result = proc_close($process);
        }

        return $result;
    }
}
