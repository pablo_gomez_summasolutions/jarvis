<?php

namespace Jarvis\Traits;

trait LocalStorageTrait
{
    /**
     * @var string
     */
    protected $localStorageFolder = '.jarvis';

    /**
     * @var string
     */
    protected $localStorageFolderCache = 'cache';

    /**
     * @var string
     */
    protected $localStorageFolderProfile = 'profile';

    /**
     * @param string $key
     * @param string $data
     * @param null|int $filemode
     * @param int $type
     * @return string
     */
    public function saveLocalStorage($key, $data, $filemode = null, $type = 1): string
    {
        $this->checkDefaultFolders();
        $folder = $this->getLocalStorageFolder();
        switch ($type) {
            case 0:
                $filename = $folder . '/' . md5($key);
                break;
            case 1:
                $filename = $folder . '/' . $this->localStorageFolderCache . '/' . md5($key);
                break;
            case 2:
                $filename = $folder . '/' . $this->localStorageFolderProfile . '/' . md5($key);
                break;
            default:
                $filename = $folder . '/' . md5($key);
        }

        // $filename = $folder . '/' . md5($key);
        file_put_contents($filename, $data);

        if (!empty($filemode)) {
            chmod($filename, $filemode);
        }

        return $filename;
    }

    /**
     * @param string $key
     * @return false|string|null
     */
    public function getLocalStorage($key)
    {
        $file = $this->getLocalStoragePath($key);
        $return = null;
        if (!empty($file)) {
            $return = file_get_contents($file);
        }

        return $return;
    }

    /**
     * @param string $key
     * @param int $type
     * @return string|null
     */
    public function getLocalStoragePath($key, $type = 1)
    {
        $folder = $this->getLocalStorageFolder();
        $filename = ($type == 1) ? $folder . '/' . $this->localStorageFolderCache . '/' . md5($key) :
            $folder . '/' . $this->localStorageFolderProfile . '/' . md5($key);

        // $filename = $folder . '/' . md5($key);
        if (file_exists($filename)) {
            return $filename;
        }

        return null;
    }

    /**
     * @param string $key
     * @return void
     */
    public function deleteLocalStorage(string $key): void
    {
        $file = (string) $this->getLocalStoragePath($key);
        if (!empty($file) && file_exists($file)) {
            chmod($file, 0777);
            unlink($file);
        }
    }

    public function checkDefaultFolders(): void
    {
        $jarvis = $this->getLocalStorageFolder();
        $baseProjectFolders = [
            'root' => $jarvis,
            'cache' => $jarvis . '/' . $this->localStorageFolderCache,
            'profile' => $jarvis . '/' . $this->localStorageFolderProfile
        ];

        foreach ($baseProjectFolders as $folders) {
            if (!file_exists($folders)) {
                mkdir($folders, 0755, true);
            }
        }
    }

    /**
     * @return string
     */
    public function getLocalStorageFolder()
    {
        return $_SERVER['HOME'] . '/' . $this->localStorageFolder;
    }
}
