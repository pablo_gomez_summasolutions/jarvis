<?php

namespace Jarvis\Util\Console\Helper;

class ScanDir
{
    /**
     * @var string[]
     */
    private static $directories;

    /**
     * @var string[]
     */
    private static $files;

    /**
     * @var string[]
     */
    private static $ext_filter;

    /**
     * @var bool
     */
    private static $recursive;

    /**
     * @return string[]
     */
    public static function scan(): array
    {
        // Initialize defaults
        self::$recursive = false;
        self::$directories = [];
        self::$files = [];
        self::$ext_filter = [];

        // Check we have minimum parameters
        if (!$args = func_get_args()) {
            die("Must provide a path string or array of path strings");
        }
        if (gettype($args[0]) != "string" && gettype($args[0]) != "array") {
            die("Must provide a path string or array of path strings");
        }

        // Check if recursive scan | default action: no sub-directories
        if (isset($args[2]) && $args[2] == true) {
            self::$recursive = true;
        }

        // Was a filter on file extensions included? | default action: return all file types
        if (isset($args[1])) {
            if (gettype($args[1]) == "array") {
                self::$ext_filter = array_map('strtolower', $args[1]);
            } elseif (gettype($args[1]) == "string") {
                self::$ext_filter[] = strtolower($args[1]);
            }
        }

        // Grab path(s)
        self::verifyPaths($args[0]);
        return self::$files;
    }

    /**
     * @param string[]|string $paths
     * @throws \Exception
     */
    private static function verifyPaths($paths): void
    {
        $path_errors = [];
        /**
         * @var string[] $parsedPaths
         */
        $parsedPaths = gettype($paths) == "string" ? [$paths] : $paths;

        foreach ($parsedPaths as $path) {
            if (is_dir($path)) {
                self::$directories[] = $path;
                $dirContents = self::findContents($path);
            } else {
                $path_errors[] = $path;
            }
        }

        if ($path_errors) {
            throw  new \Exception("The following directories do not exists: <br />" . print_r($path_errors, true));
        }
    }

    /**
     * @param string $dir
     * @return string[]
     */
    private static function findContents(string $dir): array
    {
        $result = [];
        $root = scandir((string)$dir);
        if ($root !== false) {
            foreach ($root as $value) {
                if ($value === '.' || $value === '..') {
                    continue;
                }
                if (is_file($dir . DIRECTORY_SEPARATOR . $value)) {
                    if (!self::$ext_filter ||
                        in_array(
                            strtolower(pathinfo($dir . DIRECTORY_SEPARATOR . $value, PATHINFO_EXTENSION)),
                            self::$ext_filter
                        )
                    ) {
                        self::$files[] = $result[] = $dir . DIRECTORY_SEPARATOR . $value;
                    }
                    continue;
                }
                if (self::$recursive) {
                    foreach (self::findContents($dir . DIRECTORY_SEPARATOR . $value) as $item) {
                        self::$files[] = $result[] = $item;
                    }
                }
            }
        }

        // Return required for recursive search
        return $result;
    }
}
