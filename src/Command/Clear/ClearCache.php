<?php

namespace Jarvis\Command\Clear;

use Jarvis\Command\Profile\Profile;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Command\Command;
use Jarvis\Traits\LocalStorageTrait;

use Symfony\Component\Console\Question\ChoiceQuestion;

class ClearCache extends Command
{
    use LocalStorageTrait;

    /**
     * @var string
     */
    protected static $defaultName = 'clear:cache';

    protected function configure(): void
    {
        $this->setDescription('Clear cache');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $code = 0;
        $helper = $this->getHelper('question');
        $question = new ChoiceQuestion(
            '<comment>Do you want to clear cache? [Yes]</comment>',
            ['Yes', 'No'],
            0
        );

        $question->setErrorMessage('Option %s is invalid.');
        $response = $helper->ask($input, $output, $question);

        if ($response == 'Yes') {
            $jarvis = $this->getLocalStorageFolder();
            $cachePath = $jarvis . '/' . $this->localStorageFolderCache;
            if ($this->deleteCacheData($cachePath)) {
                $output->writeln('Cache clear successful');
            }
        } else {
            $output->writeln(sprintf('Selected <info>%s</info>, aborting clear profile', $response));
            $code = 1;
        }

        return $code;
    }

    public function deleteCacheData(string $dirname): bool
    {
        $dir_handle = null;
        if (is_dir($dirname)) {
            $dir_handle = opendir($dirname);
        }
        if (!$dir_handle) {
            return false;
        }
        while ($file = readdir($dir_handle)) {
            if ($file != "." && $file != "..") {
                if (!is_dir($dirname."/".$file)) {
                    unlink($dirname."/".$file);
                } else {
                    $this->deleteCacheData($dirname.'/'.$file);
                }
            }
        }
        closedir($dir_handle);
        //rmdir($dirname);
        return true;
    }
}
