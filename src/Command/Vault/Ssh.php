<?php

namespace Jarvis\Command\Vault;

use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Jarvis\Util\General\Shell as ShellHelper;
use Symfony\Component\Console\Question\ChoiceQuestion;

class Ssh extends BaseVaultCommand
{
    /**
     * @var string
     */
    protected static $defaultName = 'vault:ssh';

    protected function configure(): void
    {
        $this->setDescription('Opens a web type access from vault server')
            ->addArgument(
                'server',
                InputArgument::OPTIONAL,
                'The server to connect to'
            )
            ->addArgument(
                'proxy',
                InputArgument::OPTIONAL,
                'The proxy server to be used to connect to the server. 
                Check vault:secrets for the list of servers'
            );

        $this->getCommandOptions($this);
    }

    protected function executeCommand(InputInterface $input, OutputInterface $output): int
    {
        $code = 0;
        $client = $this->currentClient;
        $project = $this->currentProject;
        $environment = $this->currentEnvironment;

        $proxy = $input->getArgument('proxy');
        if (!empty($proxy) && !is_string($proxy)) {
            $proxy = null;
        }

        $cache = $input->getOption(self::OPTION_NO_CACHE) ? true : false;

        $server = $input->getArgument('server');
        if (empty($server) || !is_string($server)) {
            try {
                $servers = $this->getServerLists($client, $project, $environment, 'ssh');
                if (count($servers) == 1) {
                    $server = $servers[0];
                } else {
                    $helper = $this->getHelper('question');
                    $question = new ChoiceQuestion(
                        'Please select a server (default to ' . $servers[0] . ')',
                        $servers,
                        0
                    );
                    $question->setErrorMessage('Option %s is invalid.');
                    $server = $helper->ask($input, $output, $question);
                }
            } catch (\Exception $e) {
                $output->writeln('server is not defined, use default server');
                $server = 'jumphost';
                $code = 1;
            }
        }

        $output->writeln(sprintf('<info>Connecting to server "%s"...</info>', $server));

        $host = $this->getVaultHostInfo($client, $project, $environment, $server, $proxy, $cache);
        $this->sshLogin($host);
        return $code;
    }

    /**
     * @param string[] $host
     * @return int|null
     */
    public function sshLogin($host)
    {
        $cmd = $this->getSshPrompt($host);
        $result = null;

        $process = proc_open($cmd, [STDIN, STDOUT, STDERR], $pipes);
        if ($process !== false) {
            $result = proc_close($process);
        }

        return $result;
    }

    /**
     * @param mixed $host
     * @return string
     * @throws \Exception
     */
    public function getSshPrompt($host): string
    {
        if (!empty($host['proxy'])) {
            $cmd = (string)sprintf(
                "ssh -o StrictHostKeychecking=no -o  ProxyCommand='ssh -i %s %s@%s nc %s 22' -i %s %s@%s",
                $host['proxy']['pem_file'],
                $host['proxy']['username'],
                $host['proxy']['ip'],
                $host['ip'],
                $host['pem_file'],
                $host['username'],
                $host['ip']
            );
        } elseif (!empty($host['pem_file'])) {
            $cmd = (string)sprintf(
                "ssh -i %s -o StrictHostKeychecking=no %s@%s",
                $host['pem_file'],
                $host['username'],
                $host['ip']
            );
        } else {
            $helper = new ShellHelper();
            $helper->validateSshPass();
            $cmd = (string)sprintf(
                "sshpass -p \"%s\" ssh -o StrictHostKeychecking=no %s@%s",
                $host['password'],
                $host['username'],
                $host['ip']
            );
        }

        return $cmd;
    }
}
