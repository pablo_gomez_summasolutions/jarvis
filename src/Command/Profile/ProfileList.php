<?php

namespace Jarvis\Command\Profile;

use Jarvis\Util\Console\Helper\Table\Renderer\RendererFactory;
use Jarvis\Util\Console\Helper\TableHelper;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Command\Command;

use Jarvis\Command\Profile\Profile;

class ProfileList extends Command
{
    /**
     * @var string
     */
    protected static $defaultName = 'profile:list';

    protected function configure(): void
    {
        $this->setDescription('List Profile')
            ->addOption(
                'format',
                null,
                InputOption::VALUE_OPTIONAL,
                'Output Format. One of [' . implode(',', RendererFactory::getFormats()) . ']'
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $code = 0;
        $profile = new Profile();
        $profile->checkProfileFile();
        $table = $profile->getAllProfiles();

        $format = $input->getOption('format');
        $format = is_string($format) ? $format : '';

        $tableHelper = new TableHelper($output);
        $tableHelper
            ->setHeaders(['profile', Profile::OPTION_CLIENT, Profile::OPTION_PROJECT, Profile::OPTION_ENVIRONMENT])
            ->renderByFormat($output, $table, $format);

        return $code;
    }
}
