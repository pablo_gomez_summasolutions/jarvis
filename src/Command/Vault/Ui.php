<?php

namespace Jarvis\Command\Vault;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Ui extends Command
{
    /**
     * @var string
     */
    protected static $defaultName = 'vault:ui';

    /**
     * @var string $port
     */
    protected $port = '8000';

    protected function configure(): void
    {
        $this->setDescription('Opens a web access from vault server')
            ->addOption(
                '--port',
                null,
                InputOption::VALUE_OPTIONAL,
                'Set port default: ' . $this->port
            )
            ->addArgument(
                'stop',
                InputArgument::OPTIONAL,
                'Stop vault Ui'
            );
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $code = 0;
        $port = $input->getOption('port');
        if (is_string($port)) {
            $this->port = $port;
        }

        if ($input->getArgument('stop')) {
            $shell_command = 'docker stop vault-ui';
            $message = 'Vault Ui stopped';
        } else {
            $shell_command = 'docker run --rm -d \
                                    --name vault-ui \
                                    -p ' . $this->port . ':8000 \
                                    -e VAULT_URL_DEFAULT=${VAULT_ADDR} \
                                    -e VAULT_AUTH_DEFAULT=USERNAMEPASSWORD \
                                    djenriquez/vault-ui && sleep 2 && open http://localhost:' . $this->port . '/';
            $message = 'Access to Vault Ui http://localhost:' . $this->port;
        }

        try {
            exec($shell_command, $out, $res);
            (!$res) && $output->writeln(sprintf('<info>%s</info>', $message));
        } catch (\Exception $e) {
            $output->writeln(sprintf('<error>%s</error>', $e->getMessage()));
            $code = 1;
        }

        return $code;
    }
}
