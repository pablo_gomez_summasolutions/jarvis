<?php

namespace Jarvis\Command\Vault;

use Jarvis\Command\AbstractCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class Read extends AbstractCommand
{
    // the name of the command (the part after "bin/console")
    protected static $defaultName = 'vault:read';

    protected function configure(): void
    {
        $this->setDescription('Reads a Vault secret');
    }

    protected function executeCommand(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $io->text(sprintf('Namespace is <info>%s</info>', getEnv('NAMESPACE')));
        $io->text(sprintf('Project is <info>%s</info>', getEnv('PROJECT')));

        return 0;
    }
}
