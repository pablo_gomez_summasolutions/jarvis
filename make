#!/usr/bin/env php
<?php
// The php.ini setting phar.readonly must be set to 0
$pharFile = 'build/jarvis.phar';

// clean up
if (file_exists($pharFile)) {
    unlink($pharFile);
}


$directory = __DIR__;

// Will exclude everything under these directories
$exclude = [
    'build',
    'composer.json',
    'composer.lock',
    'make',
    'README.md'
];

/**
 * @param SplFileInfo $file
 * @param mixed $key
 * @param RecursiveCallbackFilterIterator $iterator
 * @return bool True if you need to recurse or if the item is acceptable
 */
$filter = function ($file, $key, $iterator) use ($exclude) {
    if ($iterator->hasChildren() && !in_array($file->getFilename(), $exclude)) {
        return true;
    }
    return $file->isFile();
};

$innerIterator = new RecursiveDirectoryIterator(
    $directory,
    RecursiveDirectoryIterator::SKIP_DOTS
);
$iterator = new RecursiveIteratorIterator(
    new RecursiveCallbackFilterIterator($innerIterator, $filter)
);

// create phar
$p = new Phar($pharFile);

// creating our library using whole directory
$p->buildFromIterator(
    $iterator,
    $directory
);

// pointing main file which requires all classes
#$p->setDefaultStub('/src/jarvis');
$stub = "#!/usr/bin/env php\n<?php Phar::mapPhar('jarvis.phar'); require 'phar://jarvis.phar/src/jarvis'; __HALT_COMPILER();";
$p->setStub($stub);

echo "$pharFile successfully created";
