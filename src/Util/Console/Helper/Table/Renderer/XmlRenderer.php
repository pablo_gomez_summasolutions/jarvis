<?php

namespace Jarvis\Util\Console\Helper\Table\Renderer;

use Symfony\Component\Console\Output\OutputInterface;

class XmlRenderer implements RendererInterface
{
    /**
     * @param OutputInterface $output
     * @param array[] $rows
     */
    public function render(OutputInterface $output, array $rows): void
    {
        $dom = new \DOMDocument('1.0', 'UTF-8');
        $dom->formatOutput = true;
        $rootXml = $dom->createElement('table');
        $dom->appendChild($rootXml);

        foreach ($rows as $row) {
            $rowXml = $dom->createElement('row');
            foreach ($row as $key => $value) {
                $key = $this->parsedKey($key);
                $rowXml->appendChild($dom->createElement($key, $value));
            }
            $rootXml->appendChild($rowXml);
        }
        $result = $dom->saveXML();
        if ($result !== false) {
            $output->writeln($result);
        }
    }

    /**
     * @param mixed $key
     *
     * @return string
     */
    public function parsedKey($key): string
    {
        $key = preg_replace('/[^A-Za-z0-9]/u', '_', $key);

        return is_string($key) ? $key : '';
    }

    /**
     * @param mixed $value
     *
     * @return string
     */
    public function parsedValue($value): string
    {
        $value = @iconv('UTF-8', 'UTF-8//IGNORE', $value);

        return is_string($value) ? $value : '';
    }
}
