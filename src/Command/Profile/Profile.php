<?php

namespace Jarvis\Command\Profile;

use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Command\Command;

use Jarvis\Command\Profile\PhpFormatter;
use Symfony\Component\Console\Question\Question;

use Jarvis\Traits\LocalStorageTrait;
use Jarvis\Traits\TerraformTrait;
use Jarvis\Traits\VaultTrait;
use Jarvis\Traits\WarpTrait;

class Profile extends Command
{
    use TerraformTrait;
    use VaultTrait;
    use WarpTrait;

    /**
     * Arguments.
     */
    public const ARGUMENT_NAME = 'profile';
    public const OPTION_CLIENT = 'client';
    public const OPTION_PROJECT = 'project';
    public const OPTION_ENVIRONMENT = 'environment';
    public const CONFIG_FILE = 'config.php';
    public const CONFIG_FILE_FOLDER = 'profile';
    public const FILE_MODE = 0600;

    /**
     * @var string
     */
    protected static $defaultName = 'profile:create';

    /**
     * @var string
     */
    protected $currentClient;

    /**
     * @var string
     */
    protected $currentProject;

    /**
     * @var string
     */
    protected $currentEnvironment;

    /**
     * @var string
     */
    protected $profileName;


    protected function configure(): void
    {
        $this->setDescription('Set Profile')
            ->addArgument(
                self::ARGUMENT_NAME,
                InputArgument::REQUIRED,
                'Profile Name'
            )
            ->addOption(
                self::OPTION_CLIENT,
                null,
                InputOption::VALUE_OPTIONAL,
                'Client'
            )
            ->addOption(
                self::OPTION_PROJECT,
                null,
                InputOption::VALUE_OPTIONAL,
                'Project'
            )
            ->addOption(
                self::OPTION_ENVIRONMENT,
                null,
                InputOption::VALUE_OPTIONAL,
                'Environment'
            );
    }

    protected function executeCommand(InputInterface $input, OutputInterface $output): int
    {
        $code = 0;
        $profileOptionValue = $input->getArgument(self::ARGUMENT_NAME);
        $this->profileName = is_string($profileOptionValue) ? $profileOptionValue : '';

        $data = $this->getArgumentsToSave(
            $this->profileName,
            $this->currentClient,
            $this->currentProject,
            $this->currentEnvironment
        );

        $this->checkDefaultFolders();
        $this->checkProfileFile();
        $this->saveConfigProfile($data, true);

        $output->writeln(sprintf('<info>profile saved successfully  "%s"...</info>', $this->profileName));

        return $code;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $helper = $this->getHelper('question');

        $clientOptionValue = $input->getOption(self::OPTION_CLIENT);
        if (!empty($clientOptionValue) && is_string($clientOptionValue)) {
            $this->currentClient = $clientOptionValue;
        } else {
            $this->currentClient = (string) $this->getClient();
            if (empty($this->currentClient)) {
                $question = new Question('<comment>Please enter the name of the Client: </comment>');
                $this->currentClient = $helper->ask($input, $output, $question);
            }
            $output->writeln(sprintf('<info>The Client is "%s"</info>', $this->currentClient));
        }

        $projectOptionValue = $input->getOption(self::OPTION_PROJECT);
        if (!empty($projectOptionValue) && is_string($projectOptionValue)) {
            $this->currentProject = $projectOptionValue;
        } else {
            $this->currentProject = (string) $this->getProject();
            if (empty($this->currentProject)) {
                $question = new Question('<comment>Please enter the name of the Project: </comment>');
                $this->currentProject = $helper->ask($input, $output, $question);
            }
            $output->writeln(sprintf('<info>The Project is "%s"</info>', $this->currentProject));
        }

        $environmentOptionValue = $input->getOption(self::OPTION_ENVIRONMENT);
        if (!empty($environmentOptionValue) && is_string($environmentOptionValue)) {
            $this->currentEnvironment = $environmentOptionValue;
        } else {
            $this->currentEnvironment = (string) $this->getEnvironment();
            if (empty($this->currentEnvironment)) {
                $question = new Question('<comment>Please enter the name of the Environment: </comment>');
                $this->currentEnvironment = $helper->ask($input, $output, $question);
            }
            $output->writeln(sprintf('<info>The Environment is "%s"</info>', $this->currentEnvironment));
        }

        $code = $this->executeCommand($input, $output);

        return $code;
    }

    public function checkProfileFile(): void
    {
        $jarvis = $this->getLocalStorageFolder();
        $profileOldPath = $jarvis . '/' . md5(self::CONFIG_FILE);
        $profileNewPath = $jarvis . '/' . $this->localStorageFolderProfile . '/' . md5(self::CONFIG_FILE);

        if (!file_exists($profileNewPath)
            && file_exists($profileOldPath)) {
            $contents = (string) file_get_contents($profileOldPath);
            $this->saveLocalStorage(
                self::CONFIG_FILE,
                $contents,
                self::FILE_MODE,
                2
            );
            unlink($profileOldPath);
        }
    }

    /**
     * @param string $profileName
     * @return false|mixed
     */
    public function getProfileByName($profileName)
    {
        $profilePath = $this->getLocalStoragePath(self::CONFIG_FILE, 2);
        $currentData = include_once $profilePath;

        if (isset($currentData[$profileName])) {
            return $currentData[$profileName];
        } else {
            return false;
        }
    }

    /**
     * @return mixed
     */
    public function getAllProfiles()
    {
        $profilePath = $this->getLocalStoragePath(self::CONFIG_FILE, 2);
        $profileData = [];

        if (!empty($profilePath)) {
            $currentData = include_once $profilePath;

            if (is_array($currentData)) {
                foreach ($currentData as $k => $v) {
                    $index['profile'] = $k;
                    $profileData[strtolower($k)] = array_merge($index, $v);
                }
            }
        }

        return $profileData;
    }

    /**
     * @param string $profileName
     * @return bool
     */
    public function deleteProfileByName($profileName)
    {
        $profileData = $this->getAllProfiles();
        $type = 2;

        if (is_array($profileData) && isset($profileData[$profileName])) {
            $profileFilter = array_filter($profileData, function ($el) use ($profileName) {
                return $el["profile"] != $profileName;
            });

            $formatter = new PhpFormatter;
            $contents = $formatter->format($profileFilter);

            $this->saveLocalStorage(
                self::CONFIG_FILE,
                $contents,
                self::FILE_MODE,
                $type
            );

            return true;
        }

        return false;
    }

    /**
     * @param string $profile
     * @param string $client
     * @param string $project
     * @param string $environment
     * @return mixed
     */
    protected function getArgumentsToSave($profile, $client, $project, $environment)
    {
        return [
            self::CONFIG_FILE => [
                $profile => [
                    self::OPTION_CLIENT => $client,
                    self::OPTION_PROJECT => $project,
                    self::OPTION_ENVIRONMENT => $environment
                ]
            ]
        ];
    }

    /**
     * @param mixed $data
     * @param bool $override
     * @param mixed $comments
     * @return bool
     */
    protected function saveConfigProfile($data, $override = false, $comments = [])
    {
        // option 2 is profile folder
        $type = 2;
        $profilePath = $this->getLocalStoragePath(self::CONFIG_FILE, $type);
        if (empty($profilePath)) {
            $profilePath = $this->saveLocalStorage(
                self::CONFIG_FILE,
                '',
                self::FILE_MODE,
                $type
            );
        }

        $currentData = include_once $profilePath;

        if (!is_array($currentData)) {
            $currentData = [];
        }

        $contents = [];

        foreach ($data as $fileKey => $configValue) {
            if ($profilePath) {
                if ($override) {
                    $contents = array_merge($currentData, $configValue);
                } else {
                    $contents = array_replace_recursive($currentData, $configValue);
                }
            }
        }

        $formatter = new PhpFormatter;
        $contents = $formatter->format($contents, $comments);

        try {
            $this->saveLocalStorage(
                self::CONFIG_FILE,
                $contents,
                self::FILE_MODE,
                $type
            );
        } catch (\Exception $e) {
            print ($e->getMessage());
        }

        return true;
    }

    /**
     * @return string|null
     */
    public function getClient()
    {
        $return = $this->getTfClient();
        if (empty($return)) {
            $return = $this->getWarpClient();
        }

        return $return;
    }

    /**
     * @return string|null
     */
    public function getProject()
    {
        $return = $this->getTfProject();
        if (empty($return)) {
            $return = $this->getWarpProject();
        }

        return $return;
    }

    /**
     * @return string|null
     */
    public function getEnvironment()
    {
        return $this->getTfEnvironment();
    }
}
