<?php

namespace Jarvis\Command\Clear;

use Jarvis\Command\Profile\Profile;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Command\Command;
use Jarvis\Traits\LocalStorageTrait;

use Symfony\Component\Console\Question\ChoiceQuestion;

class ClearProfile extends Command
{
    use LocalStorageTrait;

    /**
     * @var string
     */
    protected static $defaultName = 'clear:profile';

    protected function configure(): void
    {
        $this->setDescription('Clear Profile');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $code = 0;
        $helper = $this->getHelper('question');
        $question = new ChoiceQuestion(
            '<comment>Do you want to clear profile Data? [Yes]</comment>',
            ['Yes', 'No'],
            0
        );

        $question->setErrorMessage('Option %s is invalid.');
        $response = $helper->ask($input, $output, $question);

        if ($response == 'Yes') {
            $jarvis = $this->getLocalStorageFolder();
            $profilePath = $jarvis . '/' . $this->localStorageFolderProfile . '/' . md5(Profile::CONFIG_FILE);
            if (unlink($profilePath)) {
                $output->writeln('Profile clear successful');
            }
        } else {
            $output->writeln(sprintf('Selected <info>%s</info>, aborting clear profile', $response));
            $code = 1;
        }

        return $code;
    }
}
