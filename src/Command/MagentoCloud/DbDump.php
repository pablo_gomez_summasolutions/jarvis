<?php

namespace Jarvis\Command\MagentoCloud;

use Jarvis\Traits\MagentoCloudTrait;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Dotenv\Dotenv;

class DbDump extends Command
{
    use MagentoCloudTrait;

    /** @var string */
    protected static $defaultName = 'magento-cloud:db-dump';

    protected function configure(): void
    {
        $this->setDescription('Generates a DB dump')
            ->addArgument('environment', InputArgument::REQUIRED, 'The environment you want to generate the dump from')
            ->addOption('download', 'd', InputOption::VALUE_NONE, 'Downloads the generated dump')
            ->addOption('only-catalog', null, InputOption::VALUE_NONE, 'Only dumps catalog tables');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $code = 0;
        $this->io = new SymfonyStyle($input, $output);

        $this->io->title('Project information');
        $this->io->text('Loading WARP configuration...');
        $dotenvFilePath = getcwd() . '/.env';
        if (!file_exists($dotenvFilePath)) {
            $this->io->text(
                sprintf(
                    '<error>Environment file not found at "%s". Is WARP installed?</error>',
                    $dotenvFilePath
                )
            );
        }

        $dotenv = new Dotenv();
        $dotenv->load($dotenvFilePath);
        $this->io->text(sprintf('<info>Loaded env file "%s"</info>', $dotenvFilePath));

        $this->io->newLine();

        $env = $input->getArgument('environment');
        $env = is_string($env) ? $env : '';

        $this->io->text(sprintf('Loading project mysql credentials for <info>%s</info>', $env));
        $dbInfo = $this->getDbInfo($env);
        $this->io->text('<info>Loaded mysql credentials info</info>');

        $this->io->newLine();

        $onlyCatalogFlag = $input->getOption('only-catalog') ? true : false;
        $dumpPath = sprintf('/tmp/%s/%s_no-definer.sql.gz', $dbInfo['path'], date('Y-m-d') . '_' . $env);
        $this->io->text(sprintf('Dumping <info>%s</info> db into <info>%s</info>...', $env, $dumpPath));
        $result = $this->dumpDb($dbInfo, $dumpPath, $env, $onlyCatalogFlag);
        $this->io->text(sprintf('Dump process completed!'));

        $downloadFlag = $input->getOption('download');
        if ($downloadFlag !== false) {
            $this->io->text(sprintf('Downloading file <info>%s</info>...', $dumpPath));
            $this->downloadFiles($env, $dumpPath);
        }

        return $code;
    }
}
