<?php

namespace Jarvis\Command\Composer;

use Jarvis\Command\Platform\InfoMagento;
use Jarvis\Traits\VaultTrait;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\HttpClient\HttpClient;

class Check extends Command
{
    use VaultTrait;

    const VAULT_SECRET_URL  = "/secret/summa/dev/palantir-access-api";
    const VAULT_KEY_URL     = "url";
    const VAULT_KEY_TOKEN   = "api_key";

    const MAGENTO_COMMUNITY = "magento/magento2-base";
    const MAGENTO_COMMERCE  = "magento/magento2-ee-base";
    const MAGENTO_B2B       = "magento/magento2-b2b-base";
    const COLUMN_NAME       = "name";
    const COLUMN_VERSION    = "version";

    const ARGUMENT_MODULE   = "module";

    /**
     * @var string
     */
    protected static $defaultName = 'composer:require';

    protected function configure(): void
    {
        $this->setDescription('Composer require')
            ->addArgument(
                self::ARGUMENT_MODULE,
                InputArgument::REQUIRED,
                'require module from private packagist'
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $code = 0;
        $secrets = $this->getVaultSecret(self::VAULT_SECRET_URL);
        $token   = $secrets[self::VAULT_KEY_TOKEN];
        $url     = $secrets[self::VAULT_KEY_URL];

        $moduleName  = $input->getArgument(self::ARGUMENT_MODULE);
        $composerUrl = $url . "/composer/";

        $dataMagento = $this->getMagentoInfo($output);

        if ($dataMagento) {
            $report = [
                "version"   => $dataMagento["version"],
                "edition"   => $dataMagento["edition"],
                "module"    => $moduleName
            ];

            /**
             * @var \Symfony\Contracts\HttpClient\HttpClientInterface $client
             */
            $client = HttpClient::create(['http_version' => '2.0']);

            $response = $client->request('POST', $composerUrl, [
                'headers' => [
                    'Content-Type' => 'application/json',
                    'X-API-KEY' => $token,
                ],
                'body' => json_encode($report, JSON_FORCE_OBJECT | JSON_UNESCAPED_SLASHES),
            ]);

            if ($response->getStatusCode() == 200) {
                $require = $response->getContent();
                $output->writeln($require);
            } else {
                $output->writeln(sprintf('<error>Error "%s" connecting to server</error>', $response->getStatusCode()));
                $code = 1;
            }
        } else {
            $output->writeln(sprintf('<error>Error processing composer lock</error>'));
            $code = 1;
        }

        return $code;
    }

    /**
     * @param OutputInterface $output
     * @return mixed
     */
    public function getMagentoInfo(OutputInterface $output)
    {
        $pathComposerLock = getcwd() . '/composer.lock';

        if (!file_exists($pathComposerLock)) {
            $output->writeln(sprintf(
                '<error>Composer lock file not found at "%s".</error>',
                $pathComposerLock
            ));

            exit;
        }

        $item = [];
        $jsonData = file_get_contents($pathComposerLock);
        if ($jsonData !== false) {
            $packages = json_decode($jsonData, true);
            $packages = $packages["packages"];

            $packages_columns = array_column($packages, self::COLUMN_NAME);

            $key_commerce = array_search(self::MAGENTO_COMMERCE, $packages_columns);
            $key_community = array_search(self::MAGENTO_COMMUNITY, $packages_columns);
            $key_b2b = array_search(self::MAGENTO_B2B, $packages_columns);

            $key = null;
            if ($key_b2b !== false) {
                $key = $key_b2b;
            } elseif ($key_commerce !== false) {
                $key = $key_commerce;
            } elseif ($key_community !== false) {
                $key = $key_community;
            }

            if ($key) {
                $item = [
                    "edition" => $packages[$key][self::COLUMN_NAME],
                    "version" => $packages[$key][self::COLUMN_VERSION]
                ];
            }
        }

        return $item;
    }
}
