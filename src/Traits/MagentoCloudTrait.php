<?php

namespace Jarvis\Traits;

use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;
use Symfony\Component\Yaml\Yaml;

trait MagentoCloudTrait
{
    /**
     * @var SymfonyStyle
     */
    protected $io;

    /**
     * @param mixed $cmd
     * @param int $timeout
     * @param bool $showProcess
     * @return false|string
     */
    protected function executeProcess($cmd, $timeout = 60, $showProcess = false)
    {
        $process = new Process($cmd);
        $process->setTimeout($timeout);

        if ($showProcess) {
            $process->start();
            foreach ($process as $type => $data) {
                if ($process::OUT === $type) {
                    $this->io->text(sprintf('<info>%s</info>', $data));
                } else { // $process::ERR === $type
                    $this->io->text(sprintf('<error>%s</error>', $data));
                }
            }
        } else {
            $process->run();
        }

        if (!$process->isSuccessful()) {
            throw new ProcessFailedException($process);
        }

        $output = $process->getOutput();

        return $output;
    }

    /**
     * @param mixed $cmd
     * @param int $timeout
     * @param bool $showProcess
     * @return false|string
     */
    protected function executeMagentoCloud($cmd, $timeout = 60, $showProcess = false)
    {
        array_unshift($cmd, 'magento-cloud');

        $output = $this->executeProcess($cmd, $timeout, $showProcess);

        return $output;
    }

    /**
     * @param string $environment
     * @return mixed
     */
    protected function getDbInfo($environment)
    {
        $action = [
            'environment:relationships',
            $environment,
            '--property=database.0',
        ];

        $rawInfo = (string) $this->executeMagentoCloud($action);
        $info = Yaml::parse($rawInfo);

        return $info;
    }

    /**
     * @param string[] $dbInfo
     * @param string $dumpPath
     * @param string $environment
     * @param bool $onlyCatalog
     * @return false|string
     */
    protected function dumpDb($dbInfo, $dumpPath, $environment, $onlyCatalog = false)
    {
        $onlyCatalogCmd = '';
        if ($onlyCatalog) {
            $onlyCatalogCmd = sprintf(
                ' $(mysql -h %s --user=%s --password=%s -D %s -Bse "SHOW TABLES LIKE \'catalog%%%%\'")',
                $dbInfo['host'],
                $dbInfo['username'],
                $dbInfo['password'],
                $dbInfo['path']
            );
        }

        $cmds = [
            'mysqldump -h %s --user=%s --password=%s --single-transaction %s' . $onlyCatalogCmd,
            'sed -e "s/DEFINER[ ]*=[ ]*[^*]*\*/\*/"',
            'gzip > %s',
        ];
        $dumpCmd = sprintf(
            implode(' | ', $cmds),
            $dbInfo['host'],
            $dbInfo['username'],
            $dbInfo['password'],
            $dbInfo['path'],
            $dumpPath
        );

        $action = [
            'ssh',
            '--environment=' . $environment,
            $dumpCmd,
        ];
        $rawInfo = $this->executeMagentoCloud($action, 3600);

        return $rawInfo;
    }

    /**
     * @param string[] $dbInfo
     * @param string $table
     * @param string $dumpPath
     * @param string $environment
     * @return false|string
     */
    protected function backupTable($dbInfo, $table, $dumpPath, $environment)
    {
        $dumpCmd = sprintf(
            'mysqldump -h %s --user=%s --password=%s --single-transaction %s %s | gzip > %s',
            $dbInfo['host'],
            $dbInfo['username'],
            $dbInfo['password'],
            $dbInfo['path'],
            $table,
            $dumpPath
        );

        $action = [
            'ssh',
            '--environment=' . $environment,
            $dumpCmd,
        ];

        $rawInfo = $this->executeMagentoCloud($action, 3600);

        return $rawInfo;
    }

    /**
     * @param string[] $dbInfo
     * @param string$dumpPath
     * @param string $environment
     * @return false|string
     */
    protected function restoreDbDump($dbInfo, $dumpPath, $environment)
    {
        $restoreCmd = sprintf(
            "zcat %s | sed -e 's/DEFINER[ ]*=[ ]*[^*]*\*/\*/' | mysql -h %s -u %s --password=%s %s",
            $dumpPath,
            $dbInfo['host'],
            $dbInfo['username'],
            $dbInfo['password'],
            $dbInfo['path']
        );

        $action = [
            'ssh',
            '--environment=' . $environment,
            $restoreCmd,
        ];

        $rawInfo = $this->executeMagentoCloud($action, 3600);

        return $rawInfo;
    }

    /**
     * @param string $environment
     * @return false|string
     */
    protected function sanitizeDb($environment)
    {
        $sqlFile = __DIR__ . '/sql/' . getEnv('FRAMEWORK') . '.sql';
        if (!file_exists($sqlFile)) {
            return sprintf('No sql file found at %s', $sqlFile);
        }

        $sqlQueries = file_get_contents($sqlFile);

        $action = [
            'sql',
            '--environment=' . $environment,
            '--relationship=database',
            $sqlQueries,
        ];

        $rawInfo = $this->executeMagentoCloud($action, 3600);

        return $rawInfo;
    }

    /**
     * @param string $sourceEnv
     * @param string $destEnv
     * @param string $files
     * @param string $destFolder
     * @param string[] $excludes
     * @return false|string
     */
    protected function rsyncFiles($sourceEnv, $destEnv, $files, $destFolder, $excludes = [])
    {
        $sshDest = $this->getSshCmd($destEnv);
        $excludeStr = '';
        if (!empty($excludes)) {
            foreach ($excludes as $exclude) {
                $excludeStr .= " --exclude $exclude";
            }
        }

        $rsynCmd = sprintf(
            'rsync -azvP %s -e "ssh -o StrictHostKeyChecking=no" %s %s:%s',
            $excludeStr,
            $files,
            $sshDest,
            $destFolder
        );

        $action = [
            'ssh',
            '--environment=' . $sourceEnv,
            $rsynCmd,
        ];

        $rawInfo = $this->executeMagentoCloud($action, 86400, true);

        return $rawInfo;
    }

    /**
     * @param string $env
     * @param string $files
     * @return false|string
     */
    protected function downloadFiles($env, $files)
    {
        $sshSource = $this->getSshCmd($env);
        $destFolder = './';
        $cmd = [
            'rsync',
            '-azvP',
            $sshSource . ':' . $files,
            $destFolder,
        ];

        $rawInfo = $this->executeProcess($cmd, 86400, true);

        return $rawInfo;
    }

    /**
     * @param string $environment
     * @return false|string
     */
    protected function getSshCmd($environment)
    {
        $action = [
            'ssh',
            '--environment=' . $environment,
            '--pipe',
        ];

        $rawInfo = $this->executeMagentoCloud($action, 3600);

        return $rawInfo;
    }
}
