<?php

namespace Jarvis\Command\Vault;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputOption;
use Vault\AuthenticationStrategies\TokenAuthenticationStrategy;
use Vault\AuthenticationStrategies\UserPassAuthenticationStrategy;
use Vault\Client;
use VaultTransports\Guzzle6Transport;

class Login extends Command
{
    /**
     * @var string
     */
    protected static $defaultName = 'vault:login';

    protected function configure(): void
    {
        $this->setDescription('Login access to vault server')
            ->addOption('server', null, InputOption::VALUE_OPTIONAL, 'Server')
            ->addOption('user', null, InputOption::VALUE_OPTIONAL, 'User')
            ->addOption('password', null, InputOption::VALUE_OPTIONAL, 'Password')
            ->addOption('token', null, InputOption::VALUE_OPTIONAL, 'login with token');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $code = 0;
        $vaultServer = !$input->getOption('server') ? $vaultServer = getEnv('VAULT_ADDR') : $input->getOption('server');
        $vaultToken = !$input->getOption('token') ? $vaultToken = getEnv('VAULT_TOKEN') : $input->getOption('token');
        $vaultUser = $input->getOption('user');
        if (!is_string($vaultUser)) {
            $vaultUser = '';
        }

        if (!is_string($vaultToken)) {
            $vaultToken = '';
        }

        $vaultPass = $input->getOption('password');
        if (!is_string($vaultPass)) {
            $vaultPass = '';
        }

        try {
            // Creating the client
            $client = new Client(new Guzzle6Transport(['base_uri' => $vaultServer]));

            if ($vaultUser && $vaultPass) {
                $authenticated = $client
                    ->setAuthenticationStrategy(new UserPassAuthenticationStrategy($vaultUser, $vaultPass))
                    ->authenticate();
            } else {
                $authenticated = $client
                    ->setAuthenticationStrategy(new TokenAuthenticationStrategy($vaultToken))
                    ->authenticate();
            }
        } catch (\Exception $e) {
            $output->writeln(sprintf(
                '<error>%s</error>',
                $e->getMessage()
            ));

            exit;
        }

        if (!$authenticated) {
            // Throw an exception or handle authentication failure.
            $output->writeln(sprintf('<error>you are not logged</error>'));
        }

        // Request exception could appear here.
        /** @var \Vault\ResponseModels\Response $response */
        $response = $client->read('/auth/token/lookup-self');

        $data = $response->getData(); // Raw array with secret's content.

        $data = json_encode($data, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);

        if (!empty($data)) {
            $output->writeln($data);
        }

        return $code;
    }
}
